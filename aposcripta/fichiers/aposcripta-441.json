{
    "id": 20454,
    "name": "aposcripta-441",
    "type": "acte",
    "url": "http:\/\/telma-chartes.irht.cnrs.fr\/aposcripta\/notice\/20454",
    "project_slug": "aposcripta",
    "project_label": "APOSCRIPTA \u2013 Lettres des papes",
    "user": "lettrespontificalesadmin",
    "last_update": "2020-10-19 16:26:49",
    "licence": "Attribution - Pas d\u2019Utilisation Commerciale - Partage dans les M\u00eames Conditions 3.0 France (CC BY-NC-SA 3.0 FR) ",
    "licence_url": "https:\/\/creativecommons.org\/licenses\/by-nc-sa\/3.0\/fr\/",
    "elements": [
        {
            "type": "pape",
            "value": "Gr\u00e9goire XI (1370-1378)"
        },
        {
            "type": "date_lieu",
            "value": "Avignon"
        },
        {
            "type": "date_libre",
            "value": "6 mai 1375"
        },
        {
            "type": "analyse",
            "value": "ACTION DE GR\u00c9GOIRE XI contre les Vaudois de la Provence, de la Savoie et du Dauphin\u00e9 (1375-1376)Nos 286-308. Voir l\u2019Introduction. p. lvii-lxv.<br\/>Le pape \u00e9tend la juridiction de l\u2019inquisiteur de Provence et de Dauphin\u00e9<a class=\"note\" href=\"#note-1\">1<\/a> sur la province de Tarentaise, o\u00f9 nul inquisiteur ne p\u00e9n\u00e8tre."
        },
        {
            "type": "transcription",
            "value": "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n Dilecto filio.. inquisitori heretice pravitatis in Arelaten., Aquen., Ebredunen. et Viennen. civitatibus, diocesibus et provinciis, et certis aliis partibus deputato, salutem, etc. \u2014 Fidedignis relatibus dolenter audivimus quod in provincia Tarantasien., limitibus tue Inquisitionis contigua, sunt nonnulle persone utriusque sexus infecte labe heretice pravitatis ; et quod hoc ex eo maxime accidit quia in illa provincia nullus est inquisitor pravitatis eiusdem. Nos igitur volentes in hac parte prout expedit providere, civitatem, diocesim et dictam provinciam Tarantasien., si nullus sit inquisitor in eis, prefate Inquisitioni adicimus per presentes, volentes ac statuentes quod in eisdem civitate, diocesi et provincia tuum officium, sicut in ceteris partibus tibi decretis, debeas exercere. Nulli ergo, etc. \u2014 Datum Avinioni, ii nonas maii, anno quinto.\n"
        },
        {
            "type": "edition",
            "value": "Jean-Marie Vidal, <i>Bullaire de l\u2019Inquisition fran\u00e7aise au XIV<sup>e<\/sup> si\u00e8cle et jusqu'\u00e0 la fin du Grand Schisme<\/i>, Paris : Letouzey et An\u00e9, 1913, n. 286, p. 407-408"
        },
        {
            "type": "sources_manuscrites",
            "value": "<i>Reg. Vat.<\/i>, t. <span class=\"smal-caps\">cclxvii<\/span>, fol. 22"
        },
        {
            "type": "sources_manuscrites",
            "value": "Wadding, <i>An. min.<\/i>, ad ann. 1375 n. <span class=\"smal-caps\">xx<\/span> (<i>in ext.<\/i>)"
        },
        {
            "type": "sources_manuscrites",
            "value": "Eubel, <i>Bull.<\/i>, t. <span class=\"smal-caps\">vi<\/span>, n. 1382 (<i>in ext.<\/i>)."
        },
        {
            "type": "notes_historiques",
            "value": "Fran\u00e7ois Borrel se trouvait d\u00e9j\u00e0 \u00e0 la t\u00eate de l\u2019Inquisition de Provence lorsque Gr\u00e9goire XI fut \u00e9lu pape. Ce moine \u00e9tait originaire de Gap. Sa ville natale lui doit la restauration de ses franchises. Il professa la th\u00e9ologie durant plusieurs ann\u00e9es, \u00e0 Avignon d\u2019abord, puis \u00e0 Marseille et ailleurs. Le 5 mai 1364, Urbain V ordonna \u00e0 Fran\u00e7ois de Cardilhac, r\u00e9gent de th\u00e9ologie \u00e0 Avignon, de lui conf\u00e9rer le titre de ma\u00eetre en th\u00e9ologie, s\u2019il l\u2019en jugeait digne, avec l\u2019usage des privil\u00e8ges accord\u00e9s aux professeurs de l\u2019Universit\u00e9 de Paris. <i>Reg. Supplic.<\/i>, t. <span class=\"smal-caps\">xl<\/span>, fol. 150 v\u00b0. Sa c\u00e9l\u00e9brit\u00e9 reste attach\u00e9e \u00e0 son \u0153uvre d\u2019inquisiteur, qui fut, on le sait, subordonn\u00e9e \u00e0 la direction souveraine et tr\u00e8s active de Gr\u00e9goire XI. Voir Introduction, p. <span class=\"smal-caps\">lxi-lxiv<\/span>. Nous publierons en appendice, d\u2019apr\u00e8s un parchemin des archives du Vatican, le texte d\u2019une sentence prononc\u00e9e par cet inquisiteur contre une femme accus\u00e9e d\u2019h\u00e9r\u00e9sie et reconnue, en fin de compte, coupable seulement d\u2019avoir n\u00e9glig\u00e9 la fr\u00e9quentation des sacrements. Borrel re\u00e7ut sa purgation canonique et lui imposa de communier aux quatre f\u00eates de No\u00ebl, de la Pentec\u00f4te, de l\u2019Assomption et de la Toussaint pendant plusieurs ann\u00e9es de suite. Elle devait se pr\u00e9senter \u00e0 la sainte table nu-pieds, <i>sine camisia<\/i>, un cierge \u00e0 la main et, apr\u00e8s avoir re\u00e7u la communion, expliquer \u00e0 haute voix \u00e0 la foule que c\u2019\u00e9tait en expiation de ses n\u00e9gligences pass\u00e9es qu\u2019elle \u00e9tait oblig\u00e9e de communier de cette sorte. Elle dut \u00e9galement payer les frais du proc\u00e8s.<br\/>On lira aussi dans l\u2019Appendice des lettres de l\u2019archev\u00eaque d\u2019Embrun, Pierre <i>Amelii<\/i>, o\u00f9 il est question de l\u2019activit\u00e9 inquisitoriale de notre religieux.<br\/>Fran\u00e7ois Borrel fut maintenu en charge par Cl\u00e9ment VII, en 1380. Il provoqua de nombreuses conversions et condamna beaucoup d\u2019h\u00e9r\u00e9tiques. Cent cinquante auraient \u00e9t\u00e9 br\u00fbl\u00e9s, \u00e0 Grenoble, en un seul jour de l\u2019an 1393, par son ordre. Lea, <i>Hist. de l\u2019Inq.<\/i>, t. <span class=\"smal-caps\">ii<\/span>, p. 183-184. En cette m\u00eame ann\u00e9e, l\u2019inquisiteur para\u00eet dans les documents, avec le titre de provincial des franciscains de Provence. Antoine Alhaudi, qu\u2019il se choisit lui-m\u00eame pour successeur \u00e0 l\u2019Inquisition, est confirm\u00e9 par le pape dans cette charge le 1<sup>er<\/sup> juin de cette ann\u00e9e (n. 325). Le 6 juin suivant, Borrel re\u00e7oit le mandat de conf\u00e9rer la ma\u00eetrise en th\u00e9ologie \u00e0 Antoine Barles, fr\u00e8re mineur. Eubel, <i>Bull.<\/i>, t. <span class=\"smal-caps\">vii<\/span>, n. 884.",
            "children": {
                "numero_note": "1"
            }
        },
        {
            "type": "date",
            "value": "1375\/05\/06",
            "children": {
                "date_deb_annee": "1375",
                "date_deb_mois": "05",
                "date_deb_jour": "06"
            },
            "range": {
                "date_apres": 13750506,
                "date_avant": 13750506
            }
        },
        {
            "type": "genre",
            "value": "Bulle"
        }
    ]
}