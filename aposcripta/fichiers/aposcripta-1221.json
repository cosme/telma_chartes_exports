{
    "id": 24069,
    "name": "aposcripta-1221",
    "type": "acte",
    "url": "http:\/\/telma-chartes.irht.cnrs.fr\/aposcripta\/notice\/24069",
    "project_slug": "aposcripta",
    "project_label": "APOSCRIPTA \u2013 Lettres des papes",
    "user": "lettrespontificalesadmin",
    "last_update": "2020-10-19 16:27:22",
    "licence": "Attribution - Pas d\u2019Utilisation Commerciale - Partage dans les M\u00eames Conditions 3.0 France (CC BY-NC-SA 3.0 FR) ",
    "licence_url": "https:\/\/creativecommons.org\/licenses\/by-nc-sa\/3.0\/fr\/",
    "elements": [
        {
            "type": "pape",
            "value": "Innocent II (1130-1143)"
        },
        {
            "type": "date_lieu",
            "value": "n.c."
        },
        {
            "type": "date_libre",
            "value": "29\u00a0avril 1138"
        },
        {
            "type": "date",
            "value": " 1138\/04\/29",
            "children": {
                "date_deb_annee": "1138",
                "date_deb_mois": "04",
                "date_deb_jour": "29"
            },
            "range": {
                "date_apres": 11380429,
                "date_avant": 11380429
            }
        },
        {
            "type": "analyse",
            "value": "Innocent\u00a0II autorise les moines de Cluny \u00e0 b\u00e2tir un monast\u00e8re pr\u00e8s de Montpellier, conform\u00e9ment au d\u00e9sir de Guillem\u00a0VI."
        },
        {
            "type": "transcription",
            "value": "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\nInnocentius episcopus, servus servorum Dei, dilecto in Cristoa filio illustri viro Guillelmo de Monte Pessulanob, salutem et apostolicam benedictionem.Sacrum et Deo amabile Cluniacense cenobium est a gloriosis principibus propensius confovendum. Ibic enim fratres religiosi, divino famulatui mancipati, gratum Deo impendunt obsequium, et puris orationibus ad Deum profusis celos penetrant, imad supernis jungunt, terrena celestibus uniunt et virorum catholicorum in eis spem habentiume peccata redimunt. Exultare quidem debet in Domino quisquis, impendendo beneficia, tam beate fraternitatis particeps fieri promeretur.Nos igitur utilitati monasterii et saluti tuef ac populi Montispessulani prospicere cupientes, piis tuis desideriis assensum prebemus, et ad honorem Dei habendi ecclesiam apud Montempessulanum eis licentiam damus, in qua divina celebrentur officia, et quicumque ibidem deliberaverint sepeliri, salvo nimirum jure matricis Ecclesie, liberam habeant facultatem.Tua igitur, dilecte in Domino fili Guillelme, interest pium votum effectui mancipare\u00a0; et, ut Cluniacense monasterium, apud te ecclesiam, possessionibus et aliis bonis dotatam, habeat, effectu prosequente complere.Datum Laterani, iii kalendas maii.\n"
        },
        {
            "type": "edition",
            "value": "Julien Rouquette, Augustin Villemagne, <i>Bullaire de l'\u00c9glise de Maguelone<\/i>, Paris, Montpellier : Picard, Valat, 1911-1914, I, n. 30-XVI, p. 48-48"
        },
        {
            "type": "remarques",
            "value": "  Date. \u2014 <span class=\"small-caps\">Jaff\u00e9<\/span> fixe cette bulle au 28\u00a0mars 1138-1143. La date du mois n\u2019est pas douteuse d\u2019apr\u00e8s le <i>Cartulaire des Guillems<\/i>\u00a0: c\u2019est le 29\u00a0avril que fut \u00e9crite cette bulle, mais nous ne connaissons aucun fait qui nous permette de la fixer, de pr\u00e9f\u00e9rence, \u00e0 l\u2019ann\u00e9e 1138. L\u2019acte de donation, qui pourrait nous diriger, ne porte aucune indication \u00e0 ce sujet (Cf. <i>Cart. des Guillems<\/i>, \u00e9dit. p.\u00a0289). <span class=\"small-caps\">Germain<\/span> lui a donn\u00e9 la date de 1138. C\u2019est l\u2019opinion accept\u00e9e par nos historiens locaux\u00a0; mais, encore une fois, nous ne connaissons aucun document qui permette de contredire <span class=\"small-caps\">Jaff\u00e9<\/span>.\n\nLe monast\u00e8re de Saint-Pierre de Clunezet fut fond\u00e9 par Guillem\u00a0VI, qui le dota, sur la rive droite du Lez, pr\u00e8s de Sauret. On sait tr\u00e8s peu de choses sur ce monast\u00e8re clunisien dont l\u2019h\u00e9ritier fut le c\u00e9l\u00e8bre monast\u00e8re de Saint-Beno\u00eet de Montpellier, fond\u00e9 par Urbain\u00a0V. La gloire de ce petit monast\u00e8re est d\u2019avoir offert l\u2019hospitalit\u00e9 dans ses murs \u00e0 Guillaume Grimoard, venu \u00e0 Montpellier pour y achever ses \u00e9tudes, qui devint Pape sous le nom d\u2019Urbain\u00a0V (Cf. sur ce monast\u00e8re\u00a0: <span class=\"small-caps\">L. Guiraud<\/span>, <i>Un monast\u00e8re clunisien \u00e0 Montpellier<\/i>, dans <i>Revue hist. du dioc\u00e8se de Montpellier<\/i>, 2<sup>e<\/sup> ann\u00e9e, pp.\u00a0245-252).\n\n[Rouquette-Villemagne]"
        },
        {
            "type": "apparat_critique",
            "value": "Ms.\u00a0: <i>Xro<\/i>\u00a0; Germain\u00a0: <i>Christo<\/i>.",
            "children": {
                "numero_note": "a"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Ms.\u00a0: <i>Monte Pessulano<\/i>\u00a0; Germain\u00a0: <i>Montepessulano<\/i>.",
            "children": {
                "numero_note": "b"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Ms.\u00a0: <i>ibi<\/i>\u00a0; Germain\u00a0: <i>hi<\/i>.",
            "children": {
                "numero_note": "c"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Ms.\u00a0: <i>ima<\/i>\u00a0; Germain\u00a0: <i>imo<\/i>.",
            "children": {
                "numero_note": "d"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Ms. et Germain\u00a0: <i>habencium<\/i>.",
            "children": {
                "numero_note": "e"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Ms.\u00a0: <i>tue<\/i>\u00a0; Germain\u00a0: <i>tui<\/i>.",
            "children": {
                "numero_note": "f"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<i>Cart. des Guillems<\/i>, fol.\u00a015\u00a0v\u00b0\u00a0; \u00e9dit, p.\u00a061",
            "children": {
                "numero_note": "0"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<span class=\"small-caps\">Gariel<\/span>, <i>Series<\/i>, t.\u00a0I, p.\u00a0176",
            "children": {
                "numero_note": "1"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<span class=\"small-caps\">Bouquet<\/span>, <i>Recueil<\/i>, t.\u00a0XV, p.\u00a0395",
            "children": {
                "numero_note": "2"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<span class=\"small-caps\">Germain<\/span>, <i>Hist. de la commune de Montpellier<\/i>, t.\u00a0I, p.\u00a0<span class=\"small-caps\">xxviii<\/span>",
            "children": {
                "numero_note": "3"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<span class=\"small-caps\">Jaff\u00e9<\/span> et <span class=\"small-caps\">Wattenbach<\/span>, n\u00b0\u00a08305",
            "children": {
                "numero_note": "4"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<span class=\"small-caps\">Migne<\/span>, <i>Patr. lat.<\/i>, t.\u00a0CLXXIX, col.\u00a0630",
            "children": {
                "numero_note": "5"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<span class=\"small-caps\">F. Fabr\u00e8ge<\/span>, <i>Hist. de Maguelone<\/i>, t.\u00a0I, p.\u00a0258.",
            "children": {
                "numero_note": "6"
            }
        },
        {
            "type": "genre",
            "value": "Bulle"
        }
    ]
}