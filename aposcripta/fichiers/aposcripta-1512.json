{
    "id": 24361,
    "name": "aposcripta-1512",
    "type": "acte",
    "url": "http:\/\/telma-chartes.irht.cnrs.fr\/aposcripta\/notice\/24361",
    "project_slug": "aposcripta",
    "project_label": "APOSCRIPTA \u2013 Lettres des papes",
    "user": "lettrespontificalesadmin",
    "last_update": "2020-10-19 16:27:28",
    "licence": "Attribution - Pas d\u2019Utilisation Commerciale - Partage dans les M\u00eames Conditions 3.0 France (CC BY-NC-SA 3.0 FR) ",
    "licence_url": "https:\/\/creativecommons.org\/licenses\/by-nc-sa\/3.0\/fr\/",
    "elements": [
        {
            "type": "pape",
            "value": "Gr\u00e9goire IX (1227-1241)"
        },
        {
            "type": "date_lieu",
            "value": "P\u00e9rouse"
        },
        {
            "type": "date_libre",
            "value": "11\u00a0juillet 1229"
        },
        {
            "type": "date",
            "value": " 1229\/07\/11",
            "children": {
                "date_deb_annee": "1229",
                "date_deb_mois": "07",
                "date_deb_jour": "11"
            },
            "range": {
                "date_apres": 12290711,
                "date_avant": 12290711
            }
        },
        {
            "type": "analyse",
            "value": "Gr\u00e9goire\u00a0IX reproche aux consuls de Montpellier, non seulement de ne pas pr\u00eater secours \u00e0 l\u2019\u00e9v\u00eaque, comte de Melgueil, mais encore de lui avoir port\u00e9 pr\u00e9judice en fermant le grau de Melgueil."
        },
        {
            "type": "transcription",
            "value": "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\nGregorius episcopus, servus servorum Deia, dilectis filiis consulibus Montispessulani, salutem et apostolicam benedictionemb.Cum Ecclesiis et viris ecclesiasticis dispendia inferuntur, et ab illis maxime qui prodesse deberent, nos non minus pro peccato illorum qui faciunt, quam pro dampno illorum qui sustinent, contristamur\u00a0; quia licet isti dampnum sustineant temporale, illi tamen gravius dampnum incurrunt, periculum videlicet animarumc.Significante sane venerabili fratre nostro, episcopo Magalonensid, accepimus quod cum vos bona et jura comitatus Melgorii, quem ipse a Romana Ecclesia sub annuo censu tenet in feudum, conservare et defenderee teneamini juramento, vosf non solum id efficere non curatis, verum etiam quemdam ipsius comitatus locum, qui gradus Melgorii dicitur, claudere in ipsius prejudicium contendentes, quasdam piscarias et sepes stagni ejusdem loci pro parte devastare presumpsistisg, et, contra juramentum prestitum temere venientes, alia dampnah ei et hominibus suis gravia et injurias irrogando, in non modicam lesionem jurium comitatus predicti, ut non solum ledatur prefatus episcopus, verum etiam Apostolica Sedes, ad quam principaliter aspiramus, et vos fame incurritis dispendium et salutisi. Devotionemj vestram monemus et hortamur attentek, per apostolica scripta mandantes, quatenus piscarias et sepes reparantes easdem, satisfaciatis eidem episcopo et ejus hominibus de dampnis et injuriis, ab ejus et hominum suoruml gravaminibus penitus quiescatis, ita quod divinam offensam et dispendium fame vestre vitetis, et nos, qui hoc dissimulare sine animarum vestrarum periculo et nostro prejudicio non possemus, providere super hoc aliter non cogamur.Datum Perusii, v nonas julii, pontificatus nostri anno tertiom.\n"
        },
        {
            "type": "edition",
            "value": "Julien Rouquette, Augustin Villemagne, <i>Bullaire de l'\u00c9glise de Maguelone<\/i>, Paris, Montpellier : Picard, Valat, 1911-1914, II, n. 329-CCXXXIII, p. 159-161"
        },
        {
            "type": "remarques",
            "value": "Cette bulle n\u2019a pas \u00e9t\u00e9 mentionn\u00e9e par <span class=\"small-caps\">Germain<\/span> dans son <i>Histoire du Commerce de Montpellier<\/i>\u00a0; cependant elle se rattache \u00e0 une s\u00e9rie de faits que cet historien a enregistr\u00e9s avec soin.\n\nLe Pape rappelle aux consuls de Montpellier le serment fait d\u2019aider l\u2019\u00e9v\u00eaque de Maguelone dans la conservation du comt\u00e9 de Melgueil. Il doit faire allusion, croyons-nous, \u00e0 l\u2019accord conclu en 1216 entre la ville et l\u2019\u00e9v\u00eaque, et que nous avons mentionn\u00e9 (Voir Tome I, p.\u00a0375). Retenons le reproche fait aux consuls par le Pape\u00a0: ils ont ferm\u00e9 le grau de Melgueil, et diminu\u00e9 d\u2019autant le comt\u00e9 de ce nom. Le pr\u00e9judice \u00e9tait tr\u00e8s grave.\n\nFaire, ici, l\u2019historique de la question\u00a0: comment nos \u00e9v\u00eaques avaient d\u2019abord re\u00e7u des comtes de Melgueil le droit de percevoir sur les navires entrant \u00e0 Maguelone ou \u00e0 Melgueil des droits d\u2019entr\u00e9e\u00a0; comment aussi la ville de Montpellier \u00e9tait, \u00e0 cette \u00e9poque, devenue un entrep\u00f4t maritime, gr\u00e2ce \u00e0 son port de Lattes, sort \u00e9videmment des limites d\u2019une simple note historique.\n\nEn devenant comte de Melgueil, l\u2019\u00e9v\u00eaque avait deux ports importants sur la c\u00f4te\u00a0: Maguelone ou Villeneuve, \u2014 nous les associons \u00e0 dessein bien qu\u2019ils fussent distincts \u2014, et Melgueil. Ces deux ports faisaient concurrence \u00e0 Montpellier. Or, nous croyons \u00eatre dans le vrai en affirmant que, dans la premi\u00e8re partie du <span class=\"small-caps\">xiii<\/span>  <sup>e<\/sup> si\u00e8cle, Montpellier, par son port de Lattes, \u00e9tait le grand entrep\u00f4t du Midi de la France. Saint Louis, il est vrai, porta un grand coup \u00e0 la prosp\u00e9rit\u00e9 commerciale de Montpellier en cr\u00e9ant le port d\u2019Aiguesmortes. Toujours est-il que nos bourgeois ont les yeux sans cesse fix\u00e9s sur la voie maritime qui fait face \u00e0 leur ville. D\u00e8s 1140 eut lieu une convention entre le seigneur de Montpellier et l\u2019\u00e9v\u00eaque de Maguelone (Cf. <i>Cart. des Guillems<\/i>, \u00e9d. <span class=\"small-caps\">Germain<\/span>, p.\u00a075)\u00a0; m\u00eame concession en avril 1181 (<i>Grand Thalamus<\/i>, fol.\u00a028\u00a0r\u00b0). Quand nos bourgeois se r\u00e9voltent contre leur seigneur, en 1142 et en 1206, c\u2019est toujours sur Lattes qu\u2019ils font porter leurs efforts. Ce n\u2019est pas seulement du ch\u00e2teau seigneurial dont ils cherchent \u00e0 s\u2019emparer, mais aussi et surtout du port qui alimente le commerce de leur ville (Cf. <span class=\"small-caps\">Germain<\/span>, <i>Hist. de la commune de Montpellier<\/i>, t.\u00a0I, p.\u00a045). En 1231, ils se font livrer par Jacques d\u2019Aragon toute la plage de Cette \u00e0 Aiguesmortes. (Cf. <i>Grand Thalamus<\/i>, fol.\u00a032\u00a0v\u00b0).\n\nCette bulle si int\u00e9ressante pour l\u2019histoire du commerce de Montpellier, apporte encore une contribution importante \u00e0 la g\u00e9ographie du littoral de la M\u00e9diterran\u00e9e \u00e0 cette \u00e9poque. A la fin de son premier volume de l\u2019<i>Histoire du Commerce de Montpellier<\/i>, <span class=\"small-caps\">Germain<\/span> a joint une carte o\u00f9 sont indiqu\u00e9s, aux diverses \u00e9poques, les diff\u00e9rents graus qui servaient de passage aux bateaux. Il place le grau de Melgueil dans la deuxi\u00e8me s\u00e9rie, c\u2019est-\u00e0-dire parmi les graus \u00ab\u00a0qui se sont ouverts \u00e0 l\u2019est de Maguelone, \u00e0 partir de l\u2019apparition du grau de Cauquillouse\u00a0\u00bb (p.\u00a0524), qui s\u2019ouvrit en 1268. Ceci est inadmissible. Le grau de Melgueil est certainement contemporain du grau de Maguelone ou Port Sarrasin, ferm\u00e9 par Arnaud quand il releva Maguelone. Il se trouve d\u2019ailleurs mentionn\u00e9 dans les actes les plus anciens, comme ceux de 1055 et 1056 (Cf. <span class=\"small-caps\">J. Rouquette<\/span> et <span class=\"small-caps\">A. Villemagne<\/span> (<i>Cart. de Maguelone<\/i>, t.\u00a0I, p.\u00a08). Au <span class=\"small-caps\">xiii<\/span>  <sup>e<\/sup> si\u00e8cle, le Lez ne d\u00e9versait pas dans la mer\u00a0: il se jetait dans l\u2019\u00e9tang de Lattes. Les vaisseaux, pour communiquer avec les ports de l\u2019int\u00e9rieur, n\u2019avaient donc que deux graus\u00a0: le grau Neuf, creus\u00e9 par le grand Arnaud au <span class=\"small-caps\">xi<\/span>  <sup>e<\/sup> si\u00e8cle, et le grau de Melgueil. En fermant celui-ci, les consuls de Montpellier obligeaient, par cons\u00e9quent, tous les navires \u00e0 passer par le grau Neuf et \u00e0 se rendre \u00e0 Lattes\u00a0; car, Maguelone, apr\u00e8s sa r\u00e9\u00e9dification, n\u2019a jamais eu aucune importance commerciale, et Villeneuve, qui \u00e9tait le port de l\u2019\u00e9v\u00eaque, mentionn\u00e9 dans les actes susdits, n\u2019a jamais pu lutter avec Melgueil, et surtout avec le port de Montpellier-Lattes. Le pr\u00e9judice caus\u00e9 \u00e0 l\u2019\u00e9v\u00eaque \u00e9tait donc consid\u00e9rable.\n\n[Rouquette-Villemagne]"
        },
        {
            "type": "apparat_critique",
            "value": "Bull.\u00a0: <i>servus servorum Dei<\/i>\u00a0; Cart.\u00a0: \u00ab\u00a0<i>servorum<\/i>\u00a0\u00bb <i>servus Dei<\/i>. Les guillemets indiquent que le mot <i>servorum<\/i> doit \u00eatre plac\u00e9 apr\u00e8s <i>servus<\/i>.",
            "children": {
                "numero_note": "a"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Bull.\u00a0: <i>benedictionem<\/i>\u00a0; Cart.\u00a0: <i>benediccionem<\/i>.",
            "children": {
                "numero_note": "b"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Bull. et Cart. portent cette le\u00e7on\u00a0: <i>nos non minus pro peccato illorum qui faciunt, tam pro dampno illorum qui sustinent, contristamur\u00a0; quia licet illi dampnum sustineant temporale, isti tamen gravius dampnum incurrunt, periculum videlicet animarum<\/i>.",
            "children": {
                "numero_note": "c"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Bull.\u00a0: <i>Magalonensi<\/i>\u00a0; Cart.\u00a0: <i>Magalanensi<\/i>.",
            "children": {
                "numero_note": "d"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Bull.\u00a0: <i>defendere<\/i>\u00a0; Cart.\u00a0: <i>deffendere<\/i>.",
            "children": {
                "numero_note": "e"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Bull.\u00a0: <i>ut<\/i>\u00a0; Cart.\u00a0: <i>vos<\/i>.",
            "children": {
                "numero_note": "f"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Bull.\u00a0: <i>pro parte devastare presumpsistis et contra<\/i>\u00a0; Cart.\u00a0: <i>pro parte<\/i> (un blanc de deux centim. environ)<i>presumpsistis contra<\/i>.",
            "children": {
                "numero_note": "g"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Bull.\u00a0: <i>temere venientes, alia dampna<\/i>\u00a0; Cart.\u00a0: \u00ab\u00a0<i>venientes<\/i>\u00a0\u00bb <i>temere alias dampna<\/i>. Le mot <i>venientes<\/i> doit \u00eatre plac\u00e9 apr\u00e8s <i>temere<\/i>.",
            "children": {
                "numero_note": "h"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Bull.\u00a0: comme le texte que nous donnons\u00a0: Cart.\u00a0: <i>irrogandi<\/i> (un blanc de 3 centim.) <i>illesionem jurium comitatus predicti non solum ledatur prefatus episcopus, verum etiam Apostolica Sedes ad quam principaliter<\/i> (un blanc de 3 centim.) <i>et vos fame incurritis dispendium et salutis<\/i>.",
            "children": {
                "numero_note": "i"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Cart.\u00a0: <i>devotionem<\/i>\u00a0; Bull.\u00a0: <i>devocionem<\/i>.",
            "children": {
                "numero_note": "j"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Cart. a ce mot\u00a0: Bull. ne l\u2019a pas.",
            "children": {
                "numero_note": "k"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Cart.\u00a0: <i>suorum hominum<\/i>\u00a0; Bull.\u00a0: <i>hominum suorum<\/i>.",
            "children": {
                "numero_note": "l"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Cart. et Bull.\u00a0: <i>tercio<\/i>.",
            "children": {
                "numero_note": "m"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<i>Cart. de Maguelone<\/i>, reg.\u00a0A, fol.\u00a011\u00a0v\u00b0",
            "children": {
                "numero_note": "0"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<i>Bull. de Maguelone<\/i>, fol.\u00a028\u00a0v\u00b0.",
            "children": {
                "numero_note": "1"
            }
        },
        {
            "type": "genre",
            "value": "Mandement (littere cum filo canapis)"
        }
    ]
}