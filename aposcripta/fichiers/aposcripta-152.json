{
    "id": 20165,
    "name": "aposcripta-152",
    "type": "acte",
    "url": "http:\/\/telma-chartes.irht.cnrs.fr\/aposcripta\/notice\/20165",
    "project_slug": "aposcripta",
    "project_label": "APOSCRIPTA \u2013 Lettres des papes",
    "user": "lettrespontificalesadmin",
    "last_update": "2020-10-19 16:26:45",
    "licence": "Attribution - Pas d\u2019Utilisation Commerciale - Partage dans les M\u00eames Conditions 3.0 France (CC BY-NC-SA 3.0 FR) ",
    "licence_url": "https:\/\/creativecommons.org\/licenses\/by-nc-sa\/3.0\/fr\/",
    "elements": [
        {
            "type": "pape",
            "value": "Cl\u00e9ment V (1305-1314)"
        },
        {
            "type": "date_lieu",
            "value": "Poitiers"
        },
        {
            "type": "date_libre",
            "value": "II idus augusti, anno tertio"
        },
        {
            "type": "analyse",
            "value": "Le pape r\u00e9voque la mesure par laquelle les cardinaux Taillefer de la Chapelle et B\u00e9renger Fr\u00e9dol, commissaires r\u00e9formateurs en mati\u00e8re inquisitoriale<a class=\"note\" href=\"#note-1\">1<\/a>, \u00e0 Carcassonne, Albi et Cordes<a class=\"note\" href=\"#note-2\">2<\/a>, avaient restreint l\u2019initiative des inquisiteurs en leur imposant l\u2019obligation de n\u2019intenter des proc\u00e8s que de concert avec les ordinaires<a class=\"note\" href=\"#note-3\">3<\/a> [Vidal]."
        },
        {
            "type": "transcription",
            "value": "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\nAd perpetuam rei memoriam. Dudum venerabili fratri nostro Petro, episcopo Penestrino, tunc tituli sancti Vitalis, et dilectis filiis nostris Berengario, tituli sanctorum Nerei et Achillei, presbiteris cardinalibus, per nostras sub certa forma litteras duximus committendum ut ipsi circa negotium inquisitionis heretice pravitatis in partibus Carcassonensibus, Albiensibus et Cordue super certis articulis seu dependentibus ab eisdem diligenter inquirerent et nonnulla etiam ordinarent ; qui, auctoritate litterarum hujusmodi quedam circa dictum officium ordinasse noscuntur. Quia vero nostre intentionis non extitit nec existit ut, occasione dicte commissionis seu alicuius mandati nostri super hiis cardinalibus ipsis facti, inquisitoribus pravitatis predicte inquirendi conjunctim vel divisim cum episcopo vel episcopis ordinariis aut sine ipsis, prout eis licet secundum canonicas xanctiones, facultas aliquatenus restringatur, nos ordinationem per quam dicti cardinales facultatem inquirendi per se divisim inquisitoribus ipsis restrinsisse dicuntur, utpote intentioni nostre et juri contrariam viribus carere decernimus et nullatenus observandam, ordinatione ipsorum cardinalium circa ceteros alios articulos in omnibus et per omnia in suo robore duratura. Nulli et cetera, nostre constitutionis et cetera. Datum Pictavis, II idus augusti, anno tertio.\n"
        },
        {
            "type": "edition",
            "value": "Ici reprise de Jean-Marie Vidal, <i>Bullaire de l\u2019Inquisition fran\u00e7aise au XIV<sup>e<\/sup> si\u00e8cle et jusqu'\u00e0 la fin du Grand Schisme<\/i>, Paris : Letouzey et An\u00e9, 1913, n. 5, p. 16-17."
        },
        {
            "type": "sources_manuscrites",
            "value": "R. CIT\u00c9 DU VATICAN, Archivio segreto Vaticano, <i>Registra Vaticana<\/i> 55, n. 568, fol. 110v.",
            "children": {
                "type_source": "Registre de la chancellerie apostolique"
            }
        },
        {
            "type": "sources_manuscrites",
            "value": "G. FRANCE, Paris, Biblioth\u00e8que nationale de France, Collection Doat, t. 34, fol. 112.",
            "children": {
                "type_source": "Copie moderne"
            }
        },
        {
            "type": "notes_historiques",
            "value": "Sur ces personnages et leur mission, voir n. 3, et notes.",
            "children": {
                "numero_note": "1"
            }
        },
        {
            "type": "notes_historiques",
            "value": "Cordes (Tarn), chef-lieu de cant., arr. de Gaillac.",
            "children": {
                "numero_note": "2"
            }
        },
        {
            "type": "notes_historiques",
            "value": "Dans plusieurs dioc\u00e8ses, bien avant qu\u2019elle f\u00fbt \u00e9rig\u00e9e en r\u00e8gle, l\u2019entente du juge ordinaire et du juge d\u00e9l\u00e9gu\u00e9 avait \u00e9t\u00e9 r\u00e9alis\u00e9e. Je ne citerai que le cas de Bernard de Castanet, instrumentant \u00e0 Albi de concert avec les inquisiteurs de Carcassonne (n. 3, note 1). Beno\u00eet XI la recommanda, le 2 mars 1304, aux inquisiteurs de Lombardie. Grandjean, <i>Registres de Beno\u00eet XI<\/i>, Paris, 1883, n. 420. Philippe le Bel, dans ses \u00e9dits de 1302 et 1304 (<i>Hist. de Lang.<\/i>, t. <span class=\"smal-caps\">x<\/span>, Preuves, col. 379-386, 428-431), la prescrivit pour la surveillance des prisons, l\u2019arrestation et la mise en libert\u00e9 sous caution des h\u00e9r\u00e9tiques, et la conduite g\u00e9n\u00e9rale du proc\u00e8s. Cl\u00e9ment V avait donn\u00e9 ordre aux cardinaux Taillefer de la Chapelle et B\u00e9renger Fr\u00e9dol (n. 3 ; Douais, <i>Documents<\/i>, t. <span class=\"smal-caps\">ii<\/span>, p. 308) de l\u2019imposer aux \u00e9v\u00eaques et aux inquisiteurs des provinces m\u00e9ridionales. Quels motifs eut le pape de revenir sur cette mesure et de la r\u00e9voquer purement et simplement ? Il est probable que la pr\u00e9sente bulle fut exp\u00e9di\u00e9e par surprise et sur les vives instances des inquisiteurs. N\u00e9anmoins le principe de l\u2019entente \u00e9tait pos\u00e9 et le concile de Vienne, en 1312, l\u2019introduisit dans le droit.<br\/>Ce fut le c\u00e9l\u00e8bre d\u00e9cret <i>Multorum<\/i> (<i>Clement.<\/i>, l. V, tit. <span class=\"smal-caps\">iii<\/span>, c. 1) qui donnait \u00e0 l\u2019\u00e9v\u00eaque les m\u00eames pouvoirs qu\u2019\u00e0 l\u2019inquisiteur. Ils pouvaient l\u2019un sans l\u2019autre citer et incarc\u00e9rer les coupables. Ils ne devaient les enfermer au mur \u00e9troit, les soumettre \u00e0 la torture et prononcer leur sentence que de conserve. La surveillance des murs leur appartiendrait \u00e0 tous deux et ils l\u2019exerceraient chacun par un gardien asserment\u00e9. La promulgation de cette constitution souleva les protestations des inquisiteurs de Toulouse et de Carcassonne, qui pr\u00e9tendirent que les proc\u00e9dures subiraient, si on l\u2019appliquait \u00e0 la lettre, des retards pr\u00e9judiciables \u00e0 la d\u00e9fense de la foi. <i>Hist. de Lang.<\/i>, t. <span class=\"smal-caps\">ix<\/span>, p. 334-337. Bernard Gui \u00e9mit l\u2019espoir de voir r\u00e9voquer cette d\u00e9cr\u00e9tale g\u00eanante. <i>Practica<\/i>, \u00e9d. Douais, p. 188. Ces r\u00e9clamations furent vaines, et les documents nous montrent les inquisiteurs n\u2019agissant jamais qu\u2019avec le concours du juge dioc\u00e9sain ou de ses d\u00e9l\u00e9gu\u00e9s. Voir Douais, <i>Documents<\/i>..., p. <span class=\"smal-caps\">cviii-cxxviii<\/span> ; J.-M. Vidal, <i>Le tribunal d\u2019Inquisition de Pamiers<\/i>, dans <i>Annales de Saint-Louis-des-Fran\u00e7ais<\/i>, 1904-1905, tirage \u00e0 part, p. 72-74, et, dans le pr\u00e9sent recueil, de nombreuses applications de cette r\u00e8gle : n. 30, 33, 39, 46-48, 52, 55, 87-88, 96-97, 100, 109, 110, 132, 139, 148, 149, 193, 230, etc.",
            "children": {
                "numero_note": "3"
            }
        },
        {
            "type": "date",
            "value": "1308\/08\/12",
            "children": {
                "date_deb_annee": "1308",
                "date_deb_mois": "08",
                "date_deb_jour": "12"
            },
            "range": {
                "date_apres": 13080812,
                "date_avant": 13080812
            }
        },
        {
            "type": "genre",
            "value": "Bulle (ad perpetuam rei memoriam)"
        },
        {
            "type": "destinataire",
            "value": "Pour m\u00e9moire perp\u00e9tuelle de la chose"
        },
        {
            "type": "edition_autre",
            "value": "Ripoll, <i>Bullarium ord. praedicatorum<\/i> Rome, 1730, II, p. 112."
        },
        {
            "type": "regeste",
            "value": "<i>Regestum Clementis Papae V<\/i>, Rome, 1885-1892, n.  2923."
        },
        {
            "type": "bibliographie_generale",
            "value": "Jean-Jacques Percin, <i>Monumenta conventus Tholosani. Opusculum de Inquisitionis nomine<\/i>, Toulouse : Joannes et Guillelmus Pech, 1693,  De Inquisitione, pars. III, c. III, p. 100 <a target=\"_blank\" href=\"https:\/\/tolosana.univ-toulouse.fr\/fr\/notice\/075570726\"> [en ligne]<\/a>."
        }
    ]
}