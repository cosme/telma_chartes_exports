{
    "id": 20244,
    "name": "aposcripta-231",
    "type": "acte",
    "url": "http:\/\/telma-chartes.irht.cnrs.fr\/aposcripta\/notice\/20244",
    "project_slug": "aposcripta",
    "project_label": "APOSCRIPTA \u2013 Lettres des papes",
    "user": "lettrespontificalesadmin",
    "last_update": "2020-10-19 16:26:46",
    "licence": "Attribution - Pas d\u2019Utilisation Commerciale - Partage dans les M\u00eames Conditions 3.0 France (CC BY-NC-SA 3.0 FR) ",
    "licence_url": "https:\/\/creativecommons.org\/licenses\/by-nc-sa\/3.0\/fr\/",
    "elements": [
        {
            "type": "pape",
            "value": "Jean XXII (1316-1334)"
        },
        {
            "type": "date_lieu",
            "value": "Avignon"
        },
        {
            "type": "date_libre",
            "value": "XII kalendas aprilis, anno undecimo"
        },
        {
            "type": "analyse",
            "value": "Jean XXII ordonne \u00e0 Michel Lemoine<a class=\"note\" href=\"#note-1\">1<\/a>, inquisiteur de Provence, de remettre aux mains de Jean Duprat<a class=\"note\" href=\"#note-2\">2<\/a>, inquisiteur de Carcassonne, Pierre Trencavel<a class=\"note\" href=\"#note-3\">3<\/a>, du dioc\u00e8se de B\u00e9ziers, condamn\u00e9 pour h\u00e9r\u00e9sie, et Andr\u00e9e, sa fille, suspecte du m\u00eame crime, tous deux \u00e9vad\u00e9s de la prison inquisitoriale de Carcassonne, et pr\u00e9sentement d\u00e9tenus dans celle de l\u2019Inquisition de Provence."
        },
        {
            "type": "transcription",
            "value": "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\nMichaeli Monachi de ordine fratrum minorum, inquisitori heretice pravitatis in partibus Provincie auctoritate apostolica deputato. \u2014 Ex insinuatione dilecti filii Johannis de Prato, ordinis fratrum predicatorum, inquisitoris heretice pravitatis in partibus Carcassonen. auctoritate apostolica deputati, nuper accepimus quod Petrus Trencavelli, Biterren. diocesis, de crimine heresis olim Carcassone in sermone publico condempnatus et muro carcerali quem fregisse temerariis ausibus postmodum et inde aufugisse dicitur, deputatus, necnon et Andrea, eiusdem Petri filia, de crimine huiusmodi vehementer suspecta et etiam fugitiva, tuis mancipati carceribus detinentur. Cum autem negotio fidei expediat quod prefati Petrus et Andrea, ut de aliis per ipsos, ut fertur, infectis ipsorumque fautoribus in eis partibus possit haberi certitudo plenior, inquisitori restituantur predicto, nos qui negotium huiusmodi ubique cupimus cooperante Domino prosperari prefati inquisitoris in hac parte supplicationibus inclinati, discretioni tue per apostolica scripta mandamus quatinus eidem inquisitori, vel eius certo nuntio predictos Petrum Trencavelli et Andream filiam eius restituere cessante difficultatis obstaculo non postponas. \u2014 Datum Avinioni, xii kalendas aprilis, anno undecimo.\n"
        },
        {
            "type": "edition",
            "value": "Jean-Marie Vidal, <i>Bullaire de l\u2019Inquisition fran\u00e7aise au XIV<sup>e<\/sup> si\u00e8cle et jusqu'\u00e0 la fin du Grand Schisme<\/i>, Paris : Letouzey et An\u00e9, 1913, n. 74, p. 124-125"
        },
        {
            "type": "sources_manuscrites",
            "value": "<i>Reg. Vat.<\/i>, t. <span class=\"smal-caps\">cxiv<\/span>, fol. 74 v\u00b0, n. 469"
        },
        {
            "type": "sources_manuscrites",
            "value": "Doat, t. <span class=\"smal-caps\">xxxv<\/span>, fol. 18"
        },
        {
            "type": "sources_manuscrites",
            "value": "Eubel, <i>Bull.<\/i>, n. 654 (<i>in ext.<\/i>)."
        },
        {
            "type": "notes_historiques",
            "value": "Voir n. 15, note 2.",
            "children": {
                "numero_note": "1"
            }
        },
        {
            "type": "notes_historiques",
            "value": "Jean Duprat, normand, d\u2019\u00c9vreux ou de Rouen, \u00e9tudiait, en 1301, la th\u00e9ologie dans le couvent des dominicains de Condom. Douais, <i>Acta capit. provinc. ord. praed.<\/i>, p. 460. En mai 1311 et 1312, les chapitres g\u00e9n\u00e9raux de Naples et de Carcassonne le d\u00e9sign\u00e8rent pour l\u2019enseignement du livre des Sentences \u00e0 Paris. Denifle, <i>Chartul.<\/i>, t. <span class=\"smal-caps\">ii<\/span>, p. 148, n. 690 ; p. 156, n. 696 ; Reichert, <i>Acta capit. gener. ord. praed.<\/i>, t. <span class=\"smal-caps\">ii<\/span>, p. 62. Il fut fait ma\u00eetre en th\u00e9ologie en 1318 ; le 13 novembre de cette ann\u00e9e, il pr\u00eata le serment de garder les privil\u00e8ges de l\u2019Universit\u00e9 de Paris. Denifle, <i>op. cit.<\/i>, p. 227, n. 776. Il assista au chapitre g\u00e9n\u00e9ral de Bordeaux, en juin 1324. Denifle, <i>op. cit.<\/i>, p. 275, n. 830. Il \u00e9tait, \u00e0 cette date, inquisiteur de Carcassonne. Qu\u00e9tif et \u00c9chard, <i>Script.<\/i>, t. <span class=\"smal-caps\">i<\/span>, p. 593-594, commettent une grande erreur quand ils retardent sa promotion \u00e0 cette charge jusqu\u2019en 1334. Ces auteurs sont, du reste, peu renseign\u00e9s sur son compte. Le 11 avril 1328, Duprat fut promu \u00e0 l\u2019\u00e9v\u00each\u00e9 d\u2019\u00c9vreux, vacant par la mort d\u2019Adam de l\u2019Isle. <i>Regest. Vat.<\/i>, t. <span class=\"smal-caps\">lxxxvi<\/span>, ep. 1437. Le chapitre g\u00e9n\u00e9ral tenu cette m\u00eame ann\u00e9e \u00e0 Toulouse imposa aux fr\u00e8res la c\u00e9l\u00e9bration d\u2019une messe aux intentions du nouvel \u00e9lu et de son successeur \u00e0 l\u2019Inquisition de Carcassonne. Reichert, <i>Acta capit. gen.<\/i>, t. <span class=\"smal-caps\">ii<\/span>, p. 184. Duprat se d\u00e9mit de son si\u00e8ge, en juin 1333. Le 30 juillet, le pape nomma \u00e0 sa place Guillaume des Essarts. <i>Reg. Vat.<\/i>, t. <span class=\"smal-caps\">civ<\/span>, ep. 618. Le 27 septembre, il conc\u00e9da \u00e0 l\u2019\u00e9v\u00eaque d\u00e9missionnaire, pour le reste de ses jours, la jouissance du manoir du <i>Sac<\/i>, d\u00e9tach\u00e9 provisoirement de la mense d\u2019\u00c9vreux, et une rente annuelle de 800 livres tournois. <i>Reg. Vat.<\/i>, t. <span class=\"smal-caps\">cvii<\/span>, ep. 154. Jean Duprat \u00e9tait mort en octobre 1335, ainsi qu\u2019il r\u00e9sulte d\u2019un document dat\u00e9 du 9 de ce mois. <i>Reg. Vat.<\/i>, t. <span class=\"smal-caps\">cxix<\/span>, ep. 849. Qu\u00e9tif et \u00c9chard le font mourir en 1338.<br\/>J\u2019ai dit qu\u2019il succ\u00e9da \u00e0 Jean de Beaune, \u00e0 l\u2019Inquisition de Carcassonne (n. 21, note 2). En ao\u00fbt 1324, il assista \u00e0 plusieurs audiences de l\u2019Inquisition de Pamiers, consacr\u00e9es \u00e0 terminer divers proc\u00e8s pendants. Les 9, 10 et 11 ao\u00fbt, il r\u00e9unit, avec l\u2019\u00e9v\u00eaque Jacques Fournier, un grand nombre de conseillers auxquels furent soumis les cas des h\u00e9r\u00e9tiques qui allaient \u00eatre jug\u00e9s. Douais, <i>La formule Communicato<\/i>, p. 20-29. Les 12 et 13 ao\u00fbt, il pronon\u00e7a, dans deux \u00ab sermons \u00bb solennels tenus dans le cimeti\u00e8re Saint-Jean et dans l\u2019\u00e9glise de Notre Dame-du-Camp, la sentence de ces accus\u00e9s et la gr\u00e2ce de nombreux p\u00e9nitents. Doat, t. <span class=\"smal-caps\">xxviii<\/span>, fol. 56-93. Le 24 f\u00e9vrier 1325 (n. st.), par son initiative, fut tenu \u00e0 Carcassonne, en pr\u00e9sence des \u00e9v\u00eaques de cette ville et de Pamiers, un autre acte de foi qu\u2019avaient pr\u00e9c\u00e9d\u00e9, le 22 et le 23, des assembl\u00e9es de jurisconsultes. Doat, t. <span class=\"smal-caps\">xxviii<\/span>, fol. 96-107 ; Mahul, <i>Cartul.<\/i> t. <span class=\"smal-caps\">v<\/span>, p. 672-674 ; Douais, <i>La formule, etc.<\/i>, p. 29-36. Le lendemain, 25 f\u00e9vrier, Duprat interrogea Guillem d\u2019Aire, de Qui\u00e9, dont le proc\u00e8s \u00e9tait pendant devant le tribunal de Pamiers Ms. Vat., lat. <i>4030<\/i>, fol. 310-312. Le 1<sup>er<\/sup> mars, il d\u00e9livra des lettres d\u2019absolution \u00e0 Jean d\u2019Avignon, de Narbonne, et lui imposa des p\u00e8lerinages. Doat, t. <span class=\"smal-caps\">xxviii<\/span>, fol. 171-174 ; Mahul, <i>op. cit.<\/i>, t. <span class=\"smal-caps\">v<\/span>, p. 675. En 1325 ou 1326, il entreprit, par ordre du pape, avec l\u2019archev\u00eaque d\u2019Aix et l\u2019inquisiteur de Besan\u00e7on, un proc\u00e8s contre Guyot Lefollet, de Frasne, h\u00e9r\u00e9tique prisonnier \u00e0 Avignon (cf. n. 65). Le 1<sup>er<\/sup> mars 1327 (n. st.), de concert avec les \u00e9v\u00eaques de Carcassonne et d\u2019Alet, l\u2019inquisiteur Pierre Brun, de Toulouse, et divers commissaires \u00e9piscopaux, il c\u00e9l\u00e9bra un <i>auto<\/i> <i>da f\u00e9<\/i> dans le march\u00e9 couvert de Carcassonne. Doat, t. <span class=\"smal-caps\">xxviii<\/span>, fol. 178 sq. ; Mahul, <i>op. cit.<\/i>, t. <span class=\"smal-caps\">v<\/span>, p. 676-683. Le 21 mars, Jean XXII lui annon\u00e7ait la remise, par Michel Lemoine, inquisiteur de Provence, de deux h\u00e9r\u00e9tiques fugitifs, Pierre Trencavel et Andr\u00e9e, sa fille, ainsi que des actes du proc\u00e8s d\u2019un pr\u00eatre relaps, Bernard Martin, dont le contenu pouvait lui \u00eatre utile. Tout en le f\u00e9licitant de son z\u00e8le contre l\u2019h\u00e9r\u00e9sie, il l\u2019exhortait \u00e0 terminer par une entente amicale un diff\u00e9rend qu\u2019il avait avec les gens de Carcassonne. L\u2019\u00e9v\u00eaque Pierre Rodier pouvait servir d\u2019arbitre (n. 74, 75, 76). Mais une discussion de comp\u00e9tence existait d\u00e9j\u00e0 entre ce pr\u00e9lat et l\u2019inquisiteur, \u00e0 propos de la punition de Barth\u00e9lemy Adalbert (n. 180, note 2), notaire de l\u2019Inquisition, coupable d\u2019exactions. Chacun d\u2019eux revendiquait pour lui-m\u00eame le droit de juger le coupable, dont la d\u00e9tention se prolongeait outre mesure. Ils finirent par s\u2019entendre et d\u00e9l\u00e9gu\u00e8rent \u00e0 l\u2019inquisiteur Pierre Brun leurs pouvoirs respectifs, le 4 mars 1328. Douais, <i>Documents<\/i>, t. <span class=\"smal-caps\">i<\/span>, p. <span class=\"smal-caps\">lxxxv-lxxxvii<\/span>. En 1326 ou 1327, Jean Duprat se disposait \u00e0 juger, \u00e0 Montpellier, une femme convaincue d\u2019h\u00e9r\u00e9sie. Le pape lui ordonna de prononcer la sentence \u00e0 Carcassonne (n. 78), sans doute, par \u00e9gard pour la fid\u00e9lit\u00e9 traditionnelle des habitants de Montpellier (n. 20, 231).<br\/>En dehors des actes inquisitoriaux de Jean Duprat, on lui attribue un <i>Commentaire sur les IV livres des Sentences<\/i> et plusieurs <i>Sermons<\/i>. Qu\u00e9tif et \u00c9chard, <i>Script.<\/i>, t. <span class=\"smal-caps\">i<\/span>, p. 593-594 ; Vidal, <i>Le trib. d\u2019Inq. de Pamiers<\/i> (tir. \u00e0 part), p. 88-91.",
            "children": {
                "numero_note": "2"
            }
        },
        {
            "type": "notes_historiques",
            "value": "Pierre Trincavel ou Trencavel, de Lieuran-Cabri\u00e8res (H\u00e9rault), \u00e9tait l\u2019un des chefs de la secte des b\u00e9guins. Il avait recueilli une somme d\u2019argent destin\u00e9e, dans sa pens\u00e9e, \u00e0 payer les frais d\u2019\u00e9migration de ses fr\u00e8res et de lui-m\u00eame en Gr\u00e8ce et \u00e0 J\u00e9rusalem, o\u00f9, pensait-il, ils trouveraient quelque s\u00fbret\u00e9. Saisi avec sa fille Andr\u00e9e, il fut jug\u00e9 et condamn\u00e9 au mur. Mais ils r\u00e9ussirent tous deux \u00e0 s\u2019\u00e9vader. <i>Hist. de Languedoc<\/i>, t. <span class=\"smal-caps\">ix<\/span>, p. 396, 399, note 2 ; Lea, <i>Hist. de l\u2019Inquis.<\/i>, \u00e9d. fr., t. <span class=\"smal-caps\">iii<\/span>, p. 90-91 ; Mahul, <i>Cartul.<\/i>, t. <span class=\"smal-caps\">v<\/span>, p. 680-681. Les registres de Doat contiennent les proc\u00e8s de plusieurs partisans de Trencavel, \u00c9tienne Gramat de B\u00e9ziers (t. <span class=\"smal-caps\">xxvii<\/span>, fol. 9), Blaise Boyer, tailleur de Narbonne (fol. 84), le pr\u00eatre Jean Roger (fol. 171), Bernard Maurin, pr\u00eatre de Narbonne (t. <span class=\"smal-caps\">xxxv<\/span>, fol. 21-47), dont il est aussi question au n. 75.",
            "children": {
                "numero_note": "3"
            }
        },
        {
            "type": "date",
            "value": "1327\/03\/21",
            "children": {
                "date_deb_annee": "1327",
                "date_deb_mois": "03",
                "date_deb_jour": "21"
            },
            "range": {
                "date_apres": 13270321,
                "date_avant": 13270321
            }
        },
        {
            "type": "genre",
            "value": "Mandement (littere cum filo canapis)"
        },
        {
            "type": "destinataire",
            "value": "L'inquisiteur de la d\u00e9pravation h\u00e9r\u00e9tique en Provence Michel Lemoine"
        }
    ]
}