{
    "id": 24077,
    "name": "aposcripta-1229",
    "type": "acte",
    "url": "http:\/\/telma-chartes.irht.cnrs.fr\/aposcripta\/notice\/24077",
    "project_slug": "aposcripta",
    "project_label": "APOSCRIPTA \u2013 Lettres des papes",
    "user": "lettrespontificalesadmin",
    "last_update": "2020-10-19 16:27:22",
    "licence": "Attribution - Pas d\u2019Utilisation Commerciale - Partage dans les M\u00eames Conditions 3.0 France (CC BY-NC-SA 3.0 FR) ",
    "licence_url": "https:\/\/creativecommons.org\/licenses\/by-nc-sa\/3.0\/fr\/",
    "elements": [
        {
            "type": "pape",
            "value": "Innocent II (1130-1143)"
        },
        {
            "type": "date_lieu",
            "value": "n.c."
        },
        {
            "type": "date_libre",
            "value": "3\u00a0mars 1142"
        },
        {
            "type": "date",
            "value": " 1142\/03\/03",
            "children": {
                "date_deb_annee": "1142",
                "date_deb_mois": "03",
                "date_deb_jour": "03"
            },
            "range": {
                "date_apres": 11420303,
                "date_avant": 11420303
            }
        },
        {
            "type": "analyse",
            "value": "Innocent\u00a0II informe Guillem\u00a0VI qu\u2019il a \u00e9crit aux \u00e9v\u00eaques de la province, leur ordonnant d\u2019excommunier le comte de Toulouse, s\u2019il pr\u00eatait encore secours aux r\u00e9volt\u00e9s de Montpellier\u00a0; quant \u00e0 l\u2019\u00e9v\u00eaque de Maguelone, son messager l\u2019instruira de ce qui a \u00e9t\u00e9 r\u00e9solu."
        },
        {
            "type": "transcription",
            "value": "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\nInnocentius episcopus, servus servorum Dei, dilecto filio G[uillelmo]a Montis Pessulanib, salutem et apostolicam benedictionem.Sicut per alia tibi scripta mandasse meminimus, de adversitate tua affectione paterna dolemus, et in quibus secundum Deum possumus, opem tibi et consilium impendimus.Inde est quod venerabilibus fratribus nostris, illius terre episcopis, per litteras nostras precipiendo mandavimus, ut Ald[efonsum]c, Tolosanum comitem, ab auxilio proditorum tuorum de Monte Pessulano desistere commoneant\u00a0; quod si contemptor extiterit, ipsum excommunicatum publice denuntientd, et in terra sua divina prohibeant officia celebrari.De episcopo vero Magalonensi quid actum sit, per nuntiume tuum cognoscere poteris.Tu vero in Domino et in potentiaf virtutis ejus confortare, qui sperantes in se non derelinquit, sed ad portum optate quietis perducit.Datum Laterani, v nonas martii.\n"
        },
        {
            "type": "edition",
            "value": "Julien Rouquette, Augustin Villemagne, <i>Bullaire de l'\u00c9glise de Maguelone<\/i>, Paris, Montpellier : Picard, Valat, 1911-1914, I, n. 38-XXI, p. 58-58"
        },
        {
            "type": "remarques",
            "value": "Dans la note que nous avons consacr\u00e9e \u00e0 la bulle d\u2019Innocent\u00a0II, d\u2019octobre 1141 (voir N\u00b0\u00a031), nous avons dit que d\u2019<span class=\"small-caps\">Aigrefeuille<\/span> avait donn\u00e9, comme cause \u00e0 la r\u00e9volte des habitants de Montpellier, le partage de la province entre le comte de Provence et le comte de Toulouse. Cette situation exacte en 1142, ne l\u2019\u00e9tait pas en 1141, au moment o\u00f9 \u00e9clata la r\u00e9volte contre Guillem\u00a0VI. En effet, en cette derni\u00e8re ann\u00e9e, il n\u2019y eut qu\u2019une guerre dans la province\u00a0: le si\u00e8ge de Toulouse par Louis le Jeune\u00a0; et Alphonse devait avoir d\u2019autres soins que celui de semer la d\u00e9sunion entre les principaux seigneurs de Languedoc.\n\nNous croyons plut\u00f4t que les circonstances favoris\u00e8rent la r\u00e9sistance des rebelles. Ici encore nous nous appuyons sur les documents pontificaux, qui ne font aucune mention de l\u2019intervention d\u2019Alphonse, en faveur des r\u00e9volt\u00e9s, pendant le cours de l\u2019ann\u00e9e 1141. Mais, vers la fin de cette ann\u00e9e, Alphonse se brouilla de nouveau avec le comte de Barcelone, alli\u00e9 de Guillem\u00a0VI (Cf. <i>Hist. g\u00e9n. de Languedoc<\/i>, t.\u00a0III, p.\u00a0720). Le comte de Barcelone \u00e9tait fr\u00e8re de Raimond-B\u00e9renger, comte de Provence et \u00e9poux de B\u00e9atrix, comtesse de Melgueil. Alphonse suscita contre le comte de Provence les seigneurs de Baux. Les habitants de Montpellier prirent parti pour le comte de Toulouse.\n\nD\u00e8s lors, dans les premiers mois de 1142, la province \u00e9tait divis\u00e9e en deux partis\u00a0: les comtes de Barcelone, de Rodez et de Provence, les vicomtes de Carcassonne, de B\u00e9ziers et de Nimes, et Guillem\u00a0VI \u00e9taient ligu\u00e9s contre le comte de Toulouse, les habitants de Montpellier et les seigneurs de Baux. \u2014 Pour de plus amples d\u00e9veloppements, nous renvoyons \u00e0 l\u2019<i>Histoire g\u00e9n\u00e9rale de Languedoc<\/i> (t.\u00a0III, pp.\u00a0720-728).\n\nTel fut, nous semble-t-il, le r\u00f4le du comte de Toulouse, ce qui lui attira l\u2019excommunication pour avoir donn\u00e9 aide et secours aux habitants de Montpellier\u00a0; mais, encore une fois, nous ne croyons pas, contrairement \u00e0 ce que dit d\u2019<span class=\"small-caps\">Aigrefeuille<\/span>, que les gens de Montpellier se soient r\u00e9volt\u00e9s, pouss\u00e9s par les Aimoins, par sympathie pour le comte de Toulouse.\n\n[Rouquette-Villemagne]"
        },
        {
            "type": "apparat_critique",
            "value": "Ms.\u00a0: <i>G.<\/i>\u00a0; Germain\u00a0: <i>Guillelmo<\/i>.",
            "children": {
                "numero_note": "a"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Ms.\u00a0: <i>Montis Pessulani<\/i>\u00a0; Germain\u00a0: <i>Montispessulam<\/i>.",
            "children": {
                "numero_note": "b"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Ms.\u00a0: <i>Ald.<\/i>\u00a0; Germain\u00a0: <i>Aldefonsum<\/i>.",
            "children": {
                "numero_note": "c"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Ms. et Germain\u00a0: <i>denuncient<\/i>.",
            "children": {
                "numero_note": "d"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Ms. et Germain\u00a0: <i>nuncium<\/i>.",
            "children": {
                "numero_note": "e"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Ms. et Germain\u00a0: <i>potencia<\/i>.",
            "children": {
                "numero_note": "f"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<i>Cart. des Guillems<\/i>, fol.\u00a012\u00a0r\u00b0\u00a0; \u00e9dit., p.\u00a041",
            "children": {
                "numero_note": "0"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<span class=\"small-caps\">Gariel<\/span>, <i>Series<\/i>, t.\u00a0I, p.\u00a0182",
            "children": {
                "numero_note": "1"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<span class=\"small-caps\">Bouquet<\/span>, <i>Recueil<\/i>, t.\u00a0XV, p.\u00a0406",
            "children": {
                "numero_note": "2"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<span class=\"small-caps\">Devic-Vaissete<\/span>, <i>Hist. g\u00e9n. de Languedoc<\/i>, t.\u00a0III, p.\u00a0721",
            "children": {
                "numero_note": "3"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<span class=\"small-caps\">Migne<\/span>, <i>Patr. lat.<\/i>, t.\u00a0CLXXIX, col.\u00a0582",
            "children": {
                "numero_note": "4"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<span class=\"small-caps\">Jaff\u00e9<\/span> et <span class=\"small-caps\">Wattenbach<\/span>, n\u00b0\u00a08203.",
            "children": {
                "numero_note": "5"
            }
        },
        {
            "type": "genre",
            "value": "Bulle"
        }
    ]
}