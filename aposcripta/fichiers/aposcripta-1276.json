{
    "id": 24124,
    "name": "aposcripta-1276",
    "type": "acte",
    "url": "http:\/\/telma-chartes.irht.cnrs.fr\/aposcripta\/notice\/24124",
    "project_slug": "aposcripta",
    "project_label": "APOSCRIPTA \u2013 Lettres des papes",
    "user": "lettrespontificalesadmin",
    "last_update": "2020-10-19 16:27:23",
    "licence": "Attribution - Pas d\u2019Utilisation Commerciale - Partage dans les M\u00eames Conditions 3.0 France (CC BY-NC-SA 3.0 FR) ",
    "licence_url": "https:\/\/creativecommons.org\/licenses\/by-nc-sa\/3.0\/fr\/",
    "elements": [
        {
            "type": "pape",
            "value": "Alexandre III (1159-1181)"
        },
        {
            "type": "date_lieu",
            "value": "n.c."
        },
        {
            "type": "date_libre",
            "value": "21\u00a0d\u00e9cembre 1167-1169"
        },
        {
            "type": "date",
            "value": "apr\u00e8s 1167\/12\/21 - avant 1169\/12\/21",
            "children": {
                "date_deb_annee": "1167",
                "date_deb_mois": "12",
                "date_deb_jour": "21",
                "date_deb_type": "apr\u00e8s",
                "date_fin_annee": "1169",
                "date_fin_mois": "12",
                "date_fin_jour": "21",
                "date_fin_type": "avant"
            },
            "range": {
                "date_apres": 11671221,
                "date_avant": 11691221
            }
        },
        {
            "type": "analyse",
            "value": "Alexandre\u00a0III prescrit aux chanoines d\u2019ob\u00e9ir au pr\u00e9v\u00f4t et de lui rendre compte de leur administration pour tout ce qui int\u00e9resse la communaut\u00e9."
        },
        {
            "type": "transcription",
            "value": "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\nAlexander episcopus, servus servorum Dei, dilectis filiis Magalonensibus canonicis, salutem et apostolicam benedictionem.Dignum est et consentaneum rationi, ut subditi prelatis suis devoti et obedientes existant, et eis de injunctis sibi administrationibus humiliter respondeant. Quapropter universitati vestre per apostolica scripta mandamus, quatenus dilecto filio nostro preposito vestro super administrationibus temporalium, ad communitatem Ecclesie vestre pertinentium, vobis commissis, sufficientem reddatis et plenariam rationem, et ei, sicut dignum est, in humilitatis spiritu obediatis. Quod nisi feceretis, timere poteritis ne super eisdem administrationibus puniamini, super quibus tenemini obedire.Datum Beneventi, xii\u00b0 kalendas januarii.\n"
        },
        {
            "type": "edition",
            "value": "Julien Rouquette, Augustin Villemagne, <i>Bullaire de l'\u00c9glise de Maguelone<\/i>, Paris, Montpellier : Picard, Valat, 1911-1914, I, n. 88-LVIII, p. 139-140"
        },
        {
            "type": "remarques",
            "value": "Quel est le sens de cette bulle\u00a0? <span class=\"small-caps\">Germain<\/span> nous semble l\u2019avoir un peu \u00e9tendu, quand il dit que les chanoines devaient rendre compte au pr\u00e9v\u00f4t \u00ab\u00a0de leur gestion comme b\u00e9n\u00e9ficiers\u00a0\u00bb. Ce sens ne nous para\u00eet pas ressortir du document\u00a0: c\u2019est en vain que nous y cherchons le mot \u00e9glise ou prieur\u00e9. Nous dirons m\u00eame que le sens qu\u2019il donne \u00e0 cette bulle nous para\u00eet erron\u00e9. Les chanoines b\u00e9n\u00e9ficiers \u00e9taient ma\u00eetres dans leurs b\u00e9n\u00e9fices, toujours en observant les r\u00e8gles ordinaires et de droit commun. En obligeant tous les b\u00e9n\u00e9ficiers \u00e0 rendre un compte exact de leur administration au pr\u00e9v\u00f4t, Alexandre\u00a0III aurait commis envers l\u2019\u00e9v\u00eaque un acte d\u2019ind\u00e9licatesse, dont un Pape n\u2019est pas capable, et donn\u00e9 au pr\u00e9v\u00f4t un pouvoir qui appartient de droit \u00e0 l\u2019\u00e9v\u00eaque.\n\nTel n\u2019est pas le sens que nous devons donner \u00e0 cette bulle. Alexandre\u00a0III nous semble d\u2019ailleurs assez pr\u00e9cis\u00a0; il parle seulement des chanoines qui administrent une partie des biens de la communaut\u00e9\u00a0: <i>super administrationibus temporalium, ad communitatem Ecclesie vestre pertinentium<\/i>. Le sens nous para\u00eet bien plus restreint, et doit s\u2019entendre des chanoines qui, comme l\u2019infirmier, le sacriste, etc., r\u00e9sidaient \u00e0 Maguelone, et qui, par leurs fonctions, devaient g\u00e9rer une partie des biens de la communaut\u00e9. Puisque le pr\u00e9v\u00f4t, ainsi que Alexandre\u00a0III l\u2019a suffisamment dit dans les bulles pr\u00e9c\u00e9dentes, a la responsabilit\u00e9 de la bonne gestion des biens de la communaut\u00e9 tout enti\u00e8re, tellement que, au cas o\u00f9 certains prieurs ne pourraient lui verser la quotit\u00e9 annuelle, il doit y suppl\u00e9er\u00a0; lui-m\u00eame ayant \u00e0 rendre compte chaque ann\u00e9e de son administration \u00e0 l\u2019\u00e9v\u00eaque et au chapitre tout entier, il est juste qu\u2019on lui rende possible l\u2019administration, en obligeant les dignitaires subalternes \u00e0 lui pr\u00e9senter leurs comptes.\n\n[Rouquette-Villemagne]"
        },
        {
            "type": "bibliographie_generale",
            "value": "<i>Priv. de Maguelone<\/i>, fol.\u00a013\u00a0v\u00b0",
            "children": {
                "numero_note": "0"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<span class=\"small-caps\">Germain<\/span>, <i>Maguelone sous ses \u00e9v\u00eaques<\/i>, p.\u00a0181",
            "children": {
                "numero_note": "1"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<span class=\"small-caps\">Jaff\u00e9<\/span> et <span class=\"small-caps\">Wattenbach<\/span>, n\u00b0\u00a011476.",
            "children": {
                "numero_note": "2"
            }
        },
        {
            "type": "genre",
            "value": "Bulle"
        }
    ]
}