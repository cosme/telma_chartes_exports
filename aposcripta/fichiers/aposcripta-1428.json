{
    "id": 24277,
    "name": "aposcripta-1428",
    "type": "acte",
    "url": "http:\/\/telma-chartes.irht.cnrs.fr\/aposcripta\/notice\/24277",
    "project_slug": "aposcripta",
    "project_label": "APOSCRIPTA \u2013 Lettres des papes",
    "user": "lettrespontificalesadmin",
    "last_update": "2020-10-19 16:27:27",
    "licence": "Attribution - Pas d\u2019Utilisation Commerciale - Partage dans les M\u00eames Conditions 3.0 France (CC BY-NC-SA 3.0 FR) ",
    "licence_url": "https:\/\/creativecommons.org\/licenses\/by-nc-sa\/3.0\/fr\/",
    "elements": [
        {
            "type": "pape",
            "value": "Honorius III (1216-1227)"
        },
        {
            "type": "date_lieu",
            "value": "n.c."
        },
        {
            "type": "date_libre",
            "value": "18\u00a0mai 1219"
        },
        {
            "type": "date",
            "value": " 1219\/05\/18",
            "children": {
                "date_deb_annee": "1219",
                "date_deb_mois": "05",
                "date_deb_jour": "18"
            },
            "range": {
                "date_apres": 12190518,
                "date_avant": 12190518
            }
        },
        {
            "type": "analyse",
            "value": "Honorius\u00a0III recommande \u00e0 Louis, fils de Philippe-Auguste, Jacques, fils de Marie de Montpellier, et les habitants de Montpellier."
        },
        {
            "type": "transcription",
            "value": "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\nHonorius episcopus, servus servorum Dei, dilecto filio nobili viro Lodoico, primogenito clarissimi in Christo filii Philippi, regis Francorum illustris, salutem et apostolicam benedictionem.Grates referimus pro te Deo, quod cum sis verus Christi miles effectus, zelus te comedat domus ejus, quemadmodum rerum demonstrant indicia, et nos certis perpendimus argumentis, dum, pr\u00e6lia volens Domini pr\u00e6liari, ad expugnandos h\u00e6reticos Albigenses, fidei nostr\u00e6 hostes, in manu forti et extenso brachio viriliter processisti, signe Crucis vivifice insignitus. Sane quia veritatis Evangelic\u00e6 dignitate attestante, dum de agro Domini colliguntur zizania, debet triticum reponendum in horreo conservari, sic te decet expugnare h\u00e6reticos, ut eos, qui christiana professione censentur et cultores divini nominis, non impugnes, sed eos potius studios\u00e6 defensionisa clippeo tuearis.Nobis quidem ex venerabilis fratris nostri, Sabinensis episcopi, et aliorum plurium proborum virorum laudabili testimonio liquet clarius, et tibi, ut credimus, dubium non existit, quod, cum villa Montispessulani, ubertate bonorum omnium taliter sit repleta, quod ei Dominus benedixisse videtur, qui de rore c\u00e6li et terr\u00e6 pinguedine abundantiam sibi dedit, dilecti consules Montispessulani et populus ejusdem vill\u00e6, supra firmam petram catholic\u00e6 fidei stabiliti, non declinant ad dexteram vel ad sinistram\u00a0; quinimo in devotione Sacrosanct\u00e6 Roman\u00e6 Ecclesi\u00e6, matris su\u00e6, firmiter et fideliter persistentes, propugnatoribus Jesu Christi crucesignatis, per terram eorum transeuntibus, ad mandatum nostrum favorabiles hactenus extiterunt.Unde dignum est et rationi consentaneum, ut ipsorum fidelitas et devotio eos tibi et aliis recommendent. Scire namque te volumus quod cum villa pr\u00e6dicta et tota terra, ad ejus spectans dominium, sint, de mandato nostro, carissimo in Christo filio nostro Jacobo, illustri regi Aragonum, restitut\u00e6, et dicti consules et populus sub Apostolic\u00e6 Sedis et nostra protectione consistant, tum ex patern\u00e6 dilectionis affectu quo sincere diligimus ipsum regem, ab inclyt\u00e6 recordationis Maria, regina Aragonum, matre ipsius, dudum apud Sedem Apostolicam decedente, cum dicta villa et aliis bonis suis Roman\u00e6 Ecclesi\u00e6 comendatum, tum etiam ob devotionem et fidelitatem eorum, ipsos manutenere propensius nos oportet.Quocirca nobilitatem tuam rogandam duximus et monendam, per apostolica scripta mandantes, quatenus consules et populum supradictos, ob reverentiam Apostolic\u00e6 Sedis et nostram habens propensius commendatos, eos in aliquo non molestes, nec permittas ab aliis, quantum in te fuerit, molestari, preces apostolicas in hac parte taliter impleturus, quod affectus devotionis sincer\u00e6, quam ad Romanam Ecclesiam et nos ipsos habere te credimus, clareat in effectu, et nos sinceritatem tuam dignis possimus in Domino laudibus commendari.Datum Rom\u00e6, apud Sanctum Petrum, xv kalendas junii, pontificatus nostri anno iii.\n"
        },
        {
            "type": "edition",
            "value": "Julien Rouquette, Augustin Villemagne, <i>Bullaire de l'\u00c9glise de Maguelone<\/i>, Paris, Montpellier : Picard, Valat, 1911-1914, II, n. 241, p. 35-37"
        },
        {
            "type": "remarques",
            "value": "Cette bulle d\u2019Honorius\u00a0III, conserv\u00e9e par <span class=\"small-caps\">Gariel<\/span>, qui, certainement, l\u2019avait puis\u00e9e dans nos Archives, nous oblige \u00e0 entrer dans certains d\u00e9tails au sujet des \u00e9v\u00e9nements qui se d\u00e9roulaient alors dans notre province.\n\nTout d\u2019abord, \u00e0 cette \u00e9poque, Montpellier \u00e9tait rentr\u00e9 sous l\u2019ob\u00e9issance du roi d\u2019Aragon. Il est remarquable que Honorius\u00a0III ne dise pas un mot de la sauvegarde accord\u00e9e par Philippe-Auguste, pour cinq ans, aux habitants, sauvegarde qui avait pris fin. Ainsi que nous l\u2019avons dit, Jacques d\u2019Aragon, dans l\u2019assembl\u00e9e de L\u00e9rida, avait pardonn\u00e9 \u00e0 ses sujets.\n\nEn cette ann\u00e9e aussi, des faits plus graves se passaient ou allaient se passer en Languedoc, qui motivaient l\u2019intervention du Pape en faveur d\u2019un enfant de onze ans. La fortune semblait sourire de nouveau \u00e0 la famille de Toulouse. A Simon de Montfort avait succ\u00e9d\u00e9 son fils, Amauri, et Raimond\u00a0VII rempla\u00e7ait l\u2019incapable Raimond\u00a0VI, encore vivant, mais qui laissait \u00e0 son fils le soin de reconqu\u00e9rir ses \u00c9tats. La partie semblait in\u00e9gale, et nul doute que le jeune comte de Toulouse n\u2019e\u00fbt bien vite rejet\u00e9 en France les Crois\u00e9s, si Amauri avait \u00e9t\u00e9 abandonn\u00e9 \u00e0 ses seules forces. Mais Philippe-Auguste veillait. Le Cap\u00e9tien, qui avait h\u00e9rit\u00e9 de ses anc\u00eatres ce sens politique si profond qu\u2019on ne peut s\u2019emp\u00eacher d\u2019admirer dans cette famille, ne pouvait laisser \u00e9chapper l\u2019occasion qui s\u2019offrait d\u2019ajouter \u00e0 sa couronne un magnifique fleuron, et pousser enfin les limites du royaume jusqu\u2019aux bords de la M\u00e9diterran\u00e9e, en enlevant \u00e0 son cousin ses \u00c9tats, ou tout au moins en en pr\u00e9parant l\u2019annexion.\n\nQuelques historiens, dom <span class=\"small-caps\">Vaissete<\/span> entre autres (<i>Hist. g\u00e9n. de Languedoc<\/i>, t.\u00a0VI, p.\u00a0619), repr\u00e9sentent Philippe-Auguste comme oppos\u00e9 \u00e0 toute entreprise de son fils contre les Albigeois. Jusqu\u2019\u00e0 quel point ces assertions sont-elles authentiques\u00a0? Les faits d\u00e9montrent que le roi de France ne retint pas son fils. En 1215, il vient dans la province en p\u00e8lerin (voir Tome I, p.\u00a0377)\u00a0; et, en 1219, au printemps, malgr\u00e9 l\u2019intervention de Raimond\u00a0VII aupr\u00e8s de son royal cousin, Louis se met en marche vers l\u2019Aquitaine. Philippe n\u2019arr\u00eate pas son fils, et Honorius\u00a0III l\u2019encourage par une bulle du 15\u00a0mai 1219 (Cf. <span class=\"small-caps\">Potthast<\/span>, n\u00b0\u00a06066).\n\nL\u2019exp\u00e9dition semblait d\u2019ailleurs r\u00e9solue depuis 1218 (Cf. <span class=\"small-caps\">Delisle<\/span>, <i>Catalogue des actes de Philippe-Auguste<\/i>, n<sup>os<\/sup>\u00a01884 et 1885, p.\u00a0415). Seul, le roi d\u2019Angleterre se pr\u00e9occupa de cette exp\u00e9dition et en \u00e9crivit au Pape. Celui-ci, par son l\u00e9gat, Bertrand, l\u2019assura que tous ses droits en Gascogne et en Aquitaine seraient sauvegard\u00e9s (Cf. <span class=\"small-caps\">Potthast<\/span>, n\u00b0\u00a06079), et que Louis ne porterait la guerre que dans le Languedoc. Amauri de Montfort accourut au-devant du prince royal, et alla en Agenais, o\u00f9 il entreprit le si\u00e8ge de Marmande. Pendant ce temps Raimond\u00a0VII ne demeura pas inactif. Il accourut au secours des assi\u00e9g\u00e9s\u00a0: la bataille de Bazi\u00e8ges lui fut favorable. Malgr\u00e9 cette victoire il ne put emp\u00eacher la jonction des deux arm\u00e9es, et Marmande fut pris. De l\u00e0, Louis alla mettre le si\u00e8ge devant Toulouse qu\u2019il ne put emporter.\n\nL\u2019ann\u00e9e 1219 ne fut donc pas aussi d\u00e9favorable \u00e0 Raimond\u00a0VII qu\u2019on aurait pu le craindre et que le Pape l\u2019aurait souhait\u00e9. Malgr\u00e9 le secours amen\u00e9 \u00e0 Amauri, celui-ci perdait du terrain\u00a0: Honorius\u00a0III \u00e9tait tromp\u00e9 dans ses esp\u00e9rances. Il devait croire, en effet, que le prince royal ne ferait qu\u2019une promenade militaire dans le Languedoc, et que, \u00e0 son approche, \u00e0 l\u2019annonce m\u00eame de son arriv\u00e9e, comme nous le verrons lors de sa troisi\u00e8me exp\u00e9dition, les villes allaient se soumettre. Il \u00e9tait \u00e0 craindre que celle de Montpellier, qui, en 1214, avait sollicit\u00e9 la sauvegarde du roi de France, qui ne s\u2019\u00e9tait soumise \u00e0 Jacques qu\u2019apr\u00e8s les sommations du Pape, ne suivit, elle aussi, l\u2019exemple des autres villes. D\u00e8s lors il fallait prendre ses pr\u00e9cautions pour pr\u00e9venir le prince royal que Montpellier \u00e9tait dans une situation sp\u00e9ciale\u00a0; que la ville appartenait au roi Jacques, recommand\u00e9 avec tant d\u2019insistance \u00e0 Innocent\u00a0III par sa m\u00e8re mourant \u00e0 Rome dans le d\u00e9nuement\u00a0; que la commune de Montpellier \u00e9tait la commune du Midi plac\u00e9e sp\u00e9cialement sous la protection de la Papaut\u00e9, et lui payant un cens annuel.\n\nTelle fut, certainement, la raison qui poussa Honorius\u00a0III \u00e0 \u00e9crire cette bulle. De fait, les \u00e9v\u00e9nements ne r\u00e9pondirent pas \u00e0 ses esp\u00e9rances\u00a0; mais un historien avis\u00e9 et impartial n\u2019oubliera pas cette nouvelle intervention du Pape envers le fils du vaincu de Muret.\n\n[Rouquette-Villemagne]"
        },
        {
            "type": "apparat_critique",
            "value": "<span class=\"small-caps\">Gariel<\/span> a omis quelques lignes. Son texte porte\u00a0: <i>sed eos potius studios\u00e6 defensionis non extitit quod cum villa Montispessulani<\/i>, etc. Nous r\u00e9tablissons le texte et le sens d\u2019apr\u00e8s les bulles pareilles adress\u00e9es au comte de Saint-Pol, Enguerrand de Couci, <i>etc.<\/i>",
            "children": {
                "numero_note": "a"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<span class=\"small-caps\">Gariel<\/span>, <i>Series<\/i>, t.\u00a0I, p.\u00a0322",
            "children": {
                "numero_note": "0"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<span class=\"small-caps\">Germain<\/span>, <i>Hist. de la commune de Montpellier<\/i>, t.\u00a0II, p.\u00a011",
            "children": {
                "numero_note": "1"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<span class=\"small-caps\">F. Fabr\u00e8ge<\/span>, <i>Hist. de Maguelone<\/i>, t.\u00a0II, p.\u00a058.",
            "children": {
                "numero_note": "2"
            }
        },
        {
            "type": "genre",
            "value": "Bulle"
        }
    ]
}