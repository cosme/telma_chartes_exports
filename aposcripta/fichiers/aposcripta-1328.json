{
    "id": 24176,
    "name": "aposcripta-1328",
    "type": "acte",
    "url": "http:\/\/telma-chartes.irht.cnrs.fr\/aposcripta\/notice\/24176",
    "project_slug": "aposcripta",
    "project_label": "APOSCRIPTA \u2013 Lettres des papes",
    "user": "lettrespontificalesadmin",
    "last_update": "2020-10-19 16:27:25",
    "licence": "Attribution - Pas d\u2019Utilisation Commerciale - Partage dans les M\u00eames Conditions 3.0 France (CC BY-NC-SA 3.0 FR) ",
    "licence_url": "https:\/\/creativecommons.org\/licenses\/by-nc-sa\/3.0\/fr\/",
    "elements": [
        {
            "type": "pape",
            "value": "C\u00e9lestin III (1191-1198)"
        },
        {
            "type": "date_lieu",
            "value": "n.c."
        },
        {
            "type": "date_libre",
            "value": "22\u00a0mai 1197"
        },
        {
            "type": "date",
            "value": " 1197\/05\/22",
            "children": {
                "date_deb_annee": "1197",
                "date_deb_mois": "05",
                "date_deb_jour": "22"
            },
            "range": {
                "date_apres": 11970522,
                "date_avant": 11970522
            }
        },
        {
            "type": "analyse",
            "value": "C\u00e9lestin\u00a0III renouvelle, en faveur du pr\u00e9v\u00f4t, la constitution d\u2019Alexandre\u00a0III touchant la nomination des archidiacres."
        },
        {
            "type": "transcription",
            "value": "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\nCelestinus episcopus, servus servorum Dei, dilecto filio Guidoni, preposito Magalonensi, salutem et apostolicam benedictionem.Cum a nobis petitur quod justum est et honestum, tam vigor equitalis quam ordo exigit rationis, ut id per sollicitudinem officii nostri ad debitum perducatur effectum.Eapropter, dilecte in Domino fili, tuis justis postulationibus grato concurrentes assensu, ad exemplar felicis recordationis Alexandri Pape, predecessoris nostri, duximus statuendum, ut, cum in Ecclesia Magalonensi archidiaconus fuerit ordinandus, episcopus consilio et consensu prepositi et archidiaconorum, prioris majoris et minoris atque sacriste, facere hoc debebit. Quod si partes forte dissenserint, ubi consilium sanioris partis accesserit, nichilominusa hoc episcopus exequatur. Statuimus etiam, ad ejusdem predecessoris nostri exemplar, ut, si forte scripta aliqua apparuerint, que in aliqua parte huic obvient institutioni, habeant suum robur in aliis\u00a0; in eo vero quod constitutioni isti visumb fuerit adversari, constitutio ista prejudicet, et illibatam teneat firmitatem.Nulli ergo omnino hominum licitum sit hanc nostre paginam constitutionis infringere, vel ei ausu temerario contraire. Si quis autem hoc attemptare presumpserit, indignationem omnipotentis Dei et beatorum Petri et Pauli, apostolorum ejus, se noverit incursurum.Datum Rome, apud Sanctum Petrum, xi kalendas junii, pontificatus nostri anno septimo.\n"
        },
        {
            "type": "edition",
            "value": "Julien Rouquette, Augustin Villemagne, <i>Bullaire de l'\u00c9glise de Maguelone<\/i>, Paris, Montpellier : Picard, Valat, 1911-1914, I, n. 140-CVII, p. 232-232"
        },
        {
            "type": "remarques",
            "value": "Cette bulle de C\u00e9lestin\u00a0III ne peut passer inaper\u00e7ue\u00a0; elle se rapporte au grand conflit qui s\u2019\u00e9tait \u00e9lev\u00e9 alors au sein de la communaut\u00e9 maguelonaise au sujet de la nomination de Pierre de Castelnau, conflit dont nous avons d\u00e9j\u00e0 parl\u00e9 (voir N\u00b0\u00a0132), et sur lequel nous allons revenir. Elle est comme un premier avertissement \u00e0 l\u2019\u00e9v\u00eaque, et comme le pr\u00e9lude de celle du 27\u00a0mai 1197 (voir N\u00b0\u00a0144) qui annulera la bulle du 22\u00a0avril 1196 (voir N\u00b0\u00a0132). On y remarquera m\u00eame une allusion \u00e0 cette derni\u00e8re. Ce ne peut \u00eatre, en effet, que cette bulle, adress\u00e9e l\u2019ann\u00e9e pr\u00e9c\u00e9dente \u00e0 l\u2019\u00e9v\u00eaque, qui est vis\u00e9e dans cette phrase\u00a0: <i>Statuimus etiam... ut si forte scripta aliqua apparuerint<\/i>, etc.\n\nAinsi C\u00e9lestin\u00a0III d\u00e9truit lui-m\u00eame sa premi\u00e8re bulle, \u2014 bient\u00f4t il nous en dira le motif \u2014, et donne raison au pr\u00e9v\u00f4t condamn\u00e9 tout d\u2019abord par lui. Il est donc bien probable que ce fut \u00e0 la fin de l\u2019ann\u00e9e 1196, peut-\u00eatre au commencement de 1197, que ce Pape annula l\u2019\u00e9lection de Pierre de Castelnau et mit \u00e0 sa place Joannin, ainsi que le rapporte Innocent\u00a0III.\n\nCe n\u2019est pas seulement au sujet de la nomination de l\u2019archidiacre que C\u00e9lestin\u00a0III devra corriger ses bulles, mais aussi au sujet de la nomination du sacriste.\n\nNous notons \u00e0 dessein cette variation dans la conduite de ce Pape\u00a0; elle est d\u2019autant plus importante, que ce qui frappe avant tout dans le <i>Bullaire<\/i> que nous publions, c\u2019est l\u2019unit\u00e9 de vue et de discipline, une s\u00fbret\u00e9 magnifique, qui est d\u2019autant plus \u00e0 admirer, qu\u2019elle s\u2019\u00e9tend sur pr\u00e8s de trois si\u00e8cles d\u2019histoire.\n\n[Rouquette-Villemagne]"
        },
        {
            "type": "apparat_critique",
            "value": "Priv.\u00a0: <i>nichilominus<\/i>\u00a0; Germain\u00a0: <i>nihilominus<\/i>.",
            "children": {
                "numero_note": "a"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Priv.\u00a0: <i>usum<\/i>\u00a0; Germain\u00a0: <i>visum<\/i>.",
            "children": {
                "numero_note": "b"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<i>Priv. de Maguelone<\/i>, fol.\u00a017\u00a0v\u00b0",
            "children": {
                "numero_note": "0"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<span class=\"small-caps\">Germain<\/span>, <i>Maguelone sous ses \u00e9v\u00eaques<\/i>, p.\u00a0180",
            "children": {
                "numero_note": "1"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<span class=\"small-caps\">Jaff\u00e9<\/span> et <span class=\"small-caps\">Wattenbach<\/span>, n\u00b0\u00a017545.",
            "children": {
                "numero_note": "2"
            }
        },
        {
            "type": "genre",
            "value": "Bulle"
        }
    ]
}