{
    "id": 23775,
    "name": "aposcripta-989",
    "type": "acte",
    "url": "http:\/\/telma-chartes.irht.cnrs.fr\/aposcripta\/notice\/23775",
    "project_slug": "aposcripta",
    "project_label": "APOSCRIPTA \u2013 Lettres des papes",
    "user": "j_theryastruc",
    "last_update": "2020-10-19 16:27:18",
    "licence": "Attribution - Pas d\u2019Utilisation Commerciale - Partage dans les M\u00eames Conditions 3.0 France (CC BY-NC-SA 3.0 FR) ",
    "licence_url": "https:\/\/creativecommons.org\/licenses\/by-nc-sa\/3.0\/fr\/",
    "elements": [
        {
            "type": "pape",
            "value": "Innocent III (1198-1216)"
        },
        {
            "type": "destinataire",
            "value": "Sanche VII le Fort, roi de Navarre"
        },
        {
            "type": "date",
            "value": " 1208\/05\/30",
            "children": {
                "date_deb_jour": "30",
                "date_deb_mois": "05",
                "date_deb_annee": "1208"
            },
            "range": {
                "date_apres": 12080530,
                "date_avant": 12080530
            }
        },
        {
            "type": "date_lieu",
            "value": "Anagni"
        },
        {
            "type": "genre",
            "value": "Mandement (littere cum filo canapis)"
        },
        {
            "type": "analyse",
            "value": "Innocent III exhorte le roi de Navarre \u00e0 pr\u00eater son concours \u00e0 l'\u00e9v\u00eaque de Calahorra et aux prieurs de Roncevaux, de Tud\u00e8le et de Tarasona, pour obliger Bernard de Monte Valdrano \u00e0 quitter l'\u00e9glise d'Artajona et \u00e0 retourner \u00e0 l'\u00e9glise de Saint-Sernin de Toulouse, dont il \u00e9tait chanoine, \u00e0 la suite d'une sentence de l'\u00e9v\u00eaque de Sabine."
        },
        {
            "type": "transcription",
            "value": "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\nInnocentius (1), episcopus, servus servorum Dei, carissimo in Christo filio, illustri r\u00e9gi Navarre (2), salutem et apostolicam benedictionem. Cum procuratores dilectorum filiorum, .. abbatis et conventus Sancti Saturnini Tolosani (3) et B[ernardus] de Monte Valdrano, canonicus ejusdem ecclesie, venerabili fratre nostro, .. Sabinensi episcopo (4), quem auditorem dederamus eisdem super ecclesia de Artaxona (5), quam idem B[ernardus] dicebatur per violentiam detinere, nuper apud Sedem apostolicam litigassent ; licet idem B[ernardus] super hoc nisus fuerit se multipliciter excusare et procuratores ipsi ad fundandam \u00eententionem suam quosdam testes produxerunt coram episcopo memorato, plurium fide dignorum super hoc testimoniales litteras exhibentes ; quia tamen ex ejusdem episcopi relatione, intelleximus evidenter eundem B[ernardum] fuisse in jure confessum se ipsius ecclesie Sancti Saturnini fore canonicum regul\u00e4rem, per eundem episcopum ipsi duximus injungendum, ut ad suam ecclesiam rediens, in qua professionem fecisse dinoscitur regularem, ibidem abbati suo debitam obedientiam exhibeat et devotam, memorata ecclesia quam detinere dinoscitur in ejusdem abbatis manibus resignata, venerabili fratri nostro, .. Calagurritano episcopo (6), et dilectis filiis, .. Roscide vallis (7) et .. Tutelanis prioribus (8), Pampilonensis et Tirasonensis diocesum, nostris dantes litteris in mandatis ut si mandatum nostrum idem B[ernardus] negligeret adimplere, ipsum ad hoc per censuram ecclesiasticam, appellatione remota, compellere non postponant, nichilominus fautores ipsius, ut a sua presumptione desistant, eadem districtione, sublato appellationis obstaculo, compellentes. Quocirca serenitatem regiam monemus attentius et hortamur quatinus eisdem judicibus ad mandatum apostolicum exequendum tribuas auxilium et favorem, tam sepedictum B[ernardum] quam fautores ipsius, ut a sua temeritate desistant, tradita tibi potestate compescens. Datum Anagnie, III kalendas junii, pontificatus nostri anno undecimo (9).\n"
        },
        {
            "type": "edition",
            "value": "L\u00e9on Cadier, \u00ab Bulles originales du XIIIe si\u00e8cle conserv\u00e9es dans les Archives de Navarre \u00bb , <i>M\u00e9langes d'arch\u00e9ologie et d'histoire<\/i>, 7, 1887, p. 268-338 <a target=\"_blank\" href=\"http:\/\/www.persee.fr\/doc\/mefr_0223-4874_1887_num_7_1_6513\"> [en ligne]<\/a>, p. 295-297."
        },
        {
            "type": "sources_manuscrites",
            "value": "E. ESPAGNE, Pampelune, Archivo General de Navarra, Comptos, Cajon IV, n. 2. Original scell\u00e9 en plomb.",
            "children": {
                "type_source": "Exp\u00e9dition"
            }
        },
        {
            "type": "notes_historiques",
            "value": "1. Innocent III, de la famille des comtes de Segni, fut \u00e9lu pape \u00e0 l'\u00e2ge de 37 ans, le 8 Janvier 1198 ; il mourut le 16 ou 17 Juillet 1216. - 2. Sanche VII le Fort, roi de Navarre. 3. L'abbaye s\u00e9culi\u00e8re de Saint-Sernin de Toulouse \u00e9tait occup\u00e9e, depuis le pontificat de Gr\u00e9goire VII, par des chanoines r\u00e9guliers de l'ordre de S. Augustin. L'abb\u00e9 de Saint-Sernin \u00e9tait, en 1208, Guillelmus de Cantesio ou de Lantesio, \u00e9lu en 1184 et mort le 5 Janvier 1212 (Gallia Christiana, t. XIII, p. 94 et 95). - 4. Le cardinal \u00e9v\u00eaque de Sabine \u00e9tait Jean, dont la souscription se trouve dans les actes d'Innocent III de 1205 \u00e0 1214. (Delisle, M\u00e9moire sur les actes d'Innocent III, (Paris, Durand, 1857, 1 vol. in-8\u00b0 p. 41). - 5. Artajona, ville de la Merindad d'Olite, aujourd'hui partido judicial de Tafalla, dioc\u00e8se de Pamplona. - 6. Gonzalez d'Agoncillo, \u00e8v\u00eaqne de Calahorra de 1199 \u00e0 1211 (Gamsr Series episcop., p. 21). 7. Roncevaux, village et monast\u00e8re de la Merindad de Sanguessa, aujourd'hui part ido judicial d'Aoiz, en Navarre. L'h\u00f4pital de Roncevaux \u00e9tait desservi au Moyen-\u00c2ge par des chanoines r\u00e9guliers de l'ordre de saint Augustin. - 8. Guillaume, prieur de Tudela, depuis 1191 jusqu'en ao\u00fbt 1215, devint \u00e9v\u00eaque de Calahorra \u00e0 cette \u00e9poque ; mais Honorius III fat oblig\u00e9 de le d\u00e9fendre contre les chanoines qui l'avaient d\u00e9pouill\u00e9 de ses insignes \u00e9piscopaux. Le prieur\u00e9 de Santa Maria de Tud\u00e8le avait acquis une grande importance \u00e0 cause de l'antagonisme des \u00e9glises de Tarazona et de Tudela, cr\u00e9\u00e9 par la s\u00e9paration des royaumes de Navarre et d'Aragon. Au milieu du XIIe si\u00e8cle, le prieur \u00e9tait \u00e9lu par l'\u00e9v\u00eaque de Tarazona et par le chapitre, et l'archidiacre de Tarazona n'exer\u00e7ait aucune juridiction sur le prieur\u00e9. Les privil\u00e8ges de l'\u00c9glise de Tud\u00e8le avaient \u00e9t\u00e9 confirm\u00e9s par le pape C\u00e9lestin III, qui augmenta les pouvoirs du prieur. Il devait \u00eatre \u00e9lu par le chapitre de Tud\u00e8le et confirm\u00e9 seulement par l'\u00e9v\u00eaque de Tarazona. A partir de 1239, le prieur de Santa Maria prit le nom de doyen de Tudela. (Espana Sagrada, t. L, p. 302 et suiv. et p. 284 \u00e0 288, [Madrid, 1866, pet. in-40]. 9. Innocent III, \u00e9lu pape le 8 ou 9 Janvier 1198, ne fut consacr\u00e9 que le 22 f\u00e9vrier de la m\u00eame ann\u00e9e (Potthast, Iiegesta Pont. Horn. t. I, p. 1 et 3).  "
        },
        {
            "type": "edition",
            "value": "Mansilla, La documentaci\u00f3n, p. 401-402."
        },
        {
            "type": "bibliographie_generale",
            "value": "Jos\u00e9 Ram\u00f3n Castro, <i>Cat\u00e1logo del Archiuo General de Navarra. Secci\u00f3n de Comptos. Documentos<\/i>. I. <i>842-1331<\/i>, Pampelune : Diputaci\u00f3n Foral de Navarra, 1952, I, n. 138."
        }
    ]
}