{
    "id": 24098,
    "name": "aposcripta-1250",
    "type": "acte",
    "url": "http:\/\/telma-chartes.irht.cnrs.fr\/aposcripta\/notice\/24098",
    "project_slug": "aposcripta",
    "project_label": "APOSCRIPTA \u2013 Lettres des papes",
    "user": "lettrespontificalesadmin",
    "last_update": "2020-10-19 16:27:23",
    "licence": "Attribution - Pas d\u2019Utilisation Commerciale - Partage dans les M\u00eames Conditions 3.0 France (CC BY-NC-SA 3.0 FR) ",
    "licence_url": "https:\/\/creativecommons.org\/licenses\/by-nc-sa\/3.0\/fr\/",
    "elements": [
        {
            "type": "pape",
            "value": "Adrien IV (1154-1159)"
        },
        {
            "type": "date_lieu",
            "value": "n.c."
        },
        {
            "type": "date_libre",
            "value": "27\u00a0octobre 1158"
        },
        {
            "type": "date",
            "value": " 1158\/10\/27",
            "children": {
                "date_deb_annee": "1158",
                "date_deb_mois": "10",
                "date_deb_jour": "27"
            },
            "range": {
                "date_apres": 11581027,
                "date_avant": 11581027
            }
        },
        {
            "type": "analyse",
            "value": "Adrien\u00a0IV bl\u00e2me l\u2019\u00e9v\u00eaque de Maguelone de ce qu\u2019il dispose, suivant son bon plaisir, des biens appartenant \u00e0 la communaut\u00e9 des chanoines, et lui ordonne, en particulier, de leur rendre la d\u00eeme de Montpelli\u00e9ret."
        },
        {
            "type": "transcription",
            "value": "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\nAdrianus episcopus, servus servorum Dei, Magalonensi episcopo, salutem et apostolicama benedictionem.Ecclesiastica utilitas hoc composcit et Sanctorum Patrum sanxit auctoritasb ut, in dispositione rerum Ecclesi\u00e6, sollicitudo episcopalis consiliumc debeat et consensum suorum requirere clericorum.Pervenit autem ad nos quod bona Magalonensis Ecclesi\u00e6, communibusd tuorum clericorum usibus deputata, contra institutionem ipsius Ecclesi\u00e6 pro tu\u00e6 voluntatis arbitrio niteris dispensare\u00a0; decimam quoque Montispessulaneti ad communitateme ipsorum canonicorum, ut nobis suggeritur, pertinentem, quam utique a quodam laico diceris redemissef nondum eam restituisti. Unde quoniam [ex]g injuncto nostri apostolatus officio munerequeh acta, que sunt corrigenda, corrigere, et suam debemus unicuique justitiam conservare, fraternitati tu\u00e6 per apostolica scripta mandando pr\u00e6cipimus quatenus sine consilioi et assensu archidiaconorum et sanioris partis capituli Ecclesi\u00e6 tu\u00e6 nihil de rebus ad eorum communitatemj pertinentibus propter institutionem ipsius Ecclesi\u00e6 nullatenus pr\u00e6sumas dispensare, et quod inde abstulisti cum integritate restituas, provisurus attentius ne super hoc clericis tuis de cetero sit materia murmurandi, et nos tibi propter hoc duriori jurisdictione scribere compellamur. Decimam preterea quam superius memoravimus si quantitatem pretii, quod in ejus redemptione dedisti, cum integro jam recepisti, vel quod citius receperis, canonicis tuis, omni excusatione postposita et dilatione, restituas.\n"
        },
        {
            "type": "edition",
            "value": "Julien Rouquette, Augustin Villemagne, <i>Bullaire de l'\u00c9glise de Maguelone<\/i>, Paris, Montpellier : Picard, Valat, 1911-1914, I, n. 59-XXXVIII, p. 88-90"
        },
        {
            "type": "remarques",
            "value": "  Date. \u2014 Nous suivons pour la date de cette bulle celle que porte notre manuscrit\u00a0: 1158, qui a soin de faire remarquer que dans le <i>Livre noir<\/i>, d\u2019o\u00f9 elle a \u00e9t\u00e9 tir\u00e9e, elle ne porte aucune date de jour ni de mois. <span class=\"small-caps\">Jaff\u00e9<\/span> et <span class=\"small-caps\">Wattenbach<\/span> donnent la suivante\u00a0: 27\u00a0octobre 1156-1158\u00a0; <span class=\"small-caps\">Migne<\/span>, le 6 des kalendes de novembre, la quatri\u00e8me ann\u00e9e du pontificat, et en faisant observer que cette derni\u00e8re date pourrait bien avoir \u00e9t\u00e9 ajout\u00e9e par une main \u00e9trang\u00e8re. \u2014 Voir ce que nous disons dans la note historique suivante.\n\nLes \u00e9glises de Montpellier avaient \u00e9t\u00e9 donn\u00e9es au chapitre de Maguelone par l\u2019\u00e9v\u00eaque Godefroid, vers l\u2019an 1080. Comment et pourquoi l\u2019\u00e9v\u00eaque Raimond avait-il voulu s\u2019emparer de ces \u00e9glises et de leurs revenus, en particulier de Saint-Denis de Montpelli\u00e9ret\u00a0? Ici, nous sommes r\u00e9duit aux conjectures pour expliquer les empi\u00e8tements de l\u2019\u00e9v\u00eaque, dont l\u2019entreprise sur Saint-Denis ne fut qu\u2019un acte isol\u00e9.\n\nPour ce dernier fait, nous avons quelques pi\u00e8ces qui jettent un peu de lumi\u00e8re sur cette bulle. <span class=\"small-caps\">Arnaud de Verdale<\/span> (\u00e9d. <span class=\"small-caps\">Germain<\/span>, p.\u00a092) nous dit que, en 1157, Raimond, \u00e9v\u00eaque de Maguelone, acquit de nombreuses possessions dans la paroisse de Saint-Denis de Montpelli\u00e9ret. L\u2019acte est cit\u00e9 aux pi\u00e8ces justificatives (p.\u00a0176), d\u2019apr\u00e8s le <i>Cartulaire de Maguelone<\/i>.\n\nCeci nous oblige \u00e0 remonter \u00e0 quelques ann\u00e9es en arri\u00e8re. Nous avons dit au commencement de cette note que Godefroid avait donn\u00e9 cette \u00e9glise aux chanoines vers 1080\u00a0: c\u2019est la date que <span class=\"small-caps\">Arnaud de Verdale<\/span> assigne \u00e0 cette donation. Mais nous avons d\u00e9j\u00e0 dit plus haut, en nous appuyant m\u00eame sur des documents pontificaux (voir N\u00b0\u00a06), que ces donations, group\u00e9es par <span class=\"small-caps\">Arnaud de Verdale<\/span>, ne peuvent avoir eu lieu la m\u00eame ann\u00e9e, puisque certaines \u00e9glises mentionn\u00e9es dans cette bulle ne se trouvent pas dans le r\u00e9cit de l\u2019\u00e9v\u00eaque du <span class=\"small-caps\">xiv<\/span>  <sup>e<\/sup> si\u00e8cle. C\u2019est en particulier le cas pour Saint-Denis de Montpelli\u00e9ret.\n\nIl nous semble bien difficile d\u2019admettre que <span class=\"small-caps\">Arnaud de Verdale<\/span> ait donn\u00e9, en 1080, cette \u00e9glise aux chanoines. Il est tr\u00e8s probable en effet que, \u00e0 cette \u00e9poque, elle avait \u00e9t\u00e9 usurp\u00e9e par Guillem\u00a0V, et que ce seigneur ne la rendit \u00e0 l\u2019\u00e9v\u00eaque qu\u2019en 1090 (Cf. <i>Cart. de Maguelone<\/i>, reg. E, fol.\u00a0111, \u00e9dit\u00e9 dans <span class=\"small-caps\">Arnaud de Verdale<\/span>, \u00e9d. <span class=\"small-caps\">Germain<\/span>, p.\u00a0176). Il y eut \u00e0 cette \u00e9poque un accord\u00a0: l\u2019\u00e9v\u00eaque partagea Montpelli\u00e9ret en trois sections\u00a0; une fut c\u00e9d\u00e9e \u00e0 Guillem\u00a0V, une autre \u00e0 Bernard Aranfred, la troisi\u00e8me resta en possession de l\u2019\u00e9v\u00eaque\u00a0; mais l\u2019\u00e9glise devint la propri\u00e9t\u00e9 de ce dernier avec le cimeti\u00e8re et ses d\u00e9pendances et le tiers seulement des d\u00eemes. <i>Guirpivit predictus Villelmus eidem episcopo Gotafredo ecclesiam de Montpestlairet... et cum tertia parte decime<\/i> (<i>Cart. de Maguelone<\/i>, reg. E, fol.\u00a0111\u00a0; Cf. aussi <i>Cart. des Guillems<\/i>, \u00e9dit. pp.\u00a069 et suiv.).\n\nNous ne pouvons \u00e9videmment, dans cette note, donner une analyse, m\u00eame sommaire, de l\u2019accord qui eut lieu cette ann\u00e9e entre Guillem\u00a0V et Godefroid. On remarquera cependant que, dans l\u2019acte cit\u00e9, l\u2019\u00e9v\u00eaque ne garde pour lui que le tiers des d\u00eemes. Les deux autres tiers furent abandonn\u00e9s \u00e0 Guillem\u00a0V et \u00e0 Bernard Aranfred, chacun pour un tiers. Godefroid dut donc c\u00e9der aux chanoines de Maguelone cette \u00e9glise telle qu\u2019il la poss\u00e9dait. Plus tard, en 1149, Raimond racheta aux descendants de Bernard Aranfred le tiers que, en vertu de la convention de 1090, ils poss\u00e9daient sur Montpelli\u00e9ret, et, enfin, il acquit d\u2019autres possessions dans cette paroisse, comme nous l\u2019avons d\u00e9j\u00e0 dit.\n\n  En quoi les chanoines furent-ils l\u00e9s\u00e9s par ces nouvelles acquisitions de l\u2019\u00e9v\u00eaque\u00a0? Celui-ci, devenu propri\u00e9taire des deux tiers de Montpelli\u00e9ret, voulut-il aussi revenir sur la d\u00e9cision de son pr\u00e9d\u00e9cesseur et enlever cette \u00e9glise au chapitre\u00a0? Nous pencherions pour cette hypoth\u00e8se, bien que nous n\u2019ayons aucun autre document que celui que nous commentons.\n\nIl nous suffit d\u2019avoir donn\u00e9 ces renseignements historiques\u00a0; le lecteur peut se former une opinion et interpr\u00e9ter la bulle d\u2019Adrien\u00a0IV.\n\nOn nous permettra cependant de faire encore ici une r\u00e9flexion. Nous avons \u00e9t\u00e9 surpris de ne pas trouver cette bulle, nous ne disons pas dans le <i>Bullaire de Maguelone<\/i> ni dans le <i>Cartulaire<\/i>, mais dans le <i>Livre des Privil\u00e8ges<\/i>, dans lequel nos chanoines ont recueilli tout ce qui \u00e9tait en leur faveur, et tout ce qui \u00e9tait contre l\u2019\u00e9v\u00eaque. On comprend que cette bulle de bl\u00e2me ne figure pas dans le <i>Cartulaire<\/i>, d\u2019o\u00f9 elle aurait pass\u00e9 dans le <i>Bullaire<\/i>. Nos \u00e9v\u00eaques ont eu soin de ne pas faire collection de pareils documents, et nous comprenons parfaitement que celui-ci ne se trouve pas dans le <i>Cartulaire<\/i>, pas plus que quelques autres qui ne sont pas \u00e0 leur louange, par exemple, la bulle d\u2019Alexandre\u00a0III \u00e0 Jean de Montlaur.\n\nCela nous porterait \u00e0 croire que les torts du pr\u00e9lat ne furent pas aussi graves que le Pape le dit\u00a0; peut-\u00eatre cette note servira-t-elle \u00e0 expliquer un peu la conduite ou, du moins, \u00e0 att\u00e9nuer les torts d\u2019un \u00e9v\u00eaque qui fut un des grands bienfaiteurs du chapitre.\n\n[Rouquette-Villemagne]"
        },
        {
            "type": "apparat_critique",
            "value": "Ms.\u00a0: <i>acceptam<\/i>.",
            "children": {
                "numero_note": "a"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Ms.\u00a0: <i>authoritas<\/i>.",
            "children": {
                "numero_note": "b"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Ms.\u00a0: <i>concilium<\/i>.",
            "children": {
                "numero_note": "c"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Ms.\u00a0: <i>comunibus<\/i>.",
            "children": {
                "numero_note": "d"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Ms.\u00a0: <i>comunitatem<\/i>.",
            "children": {
                "numero_note": "e"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Ms.\u00a0: <i>redimisse<\/i>.",
            "children": {
                "numero_note": "f"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Ce mot manque dans le manuscrit.",
            "children": {
                "numero_note": "g"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Ms.\u00a0: <i>munusque<\/i>.",
            "children": {
                "numero_note": "h"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Ms.\u00a0: <i>consilio<\/i>.",
            "children": {
                "numero_note": "i"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Ms.\u00a0: <i>comunitatem<\/i>.",
            "children": {
                "numero_note": "j"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "Nous donnons cette bulle d\u2019apr\u00e8s le seul manuscrit que nous ayons eu entre les mains, et qui a \u00e9t\u00e9 mis \u00e0 notre disposition par l\u2019\u00e9v\u00each\u00e9 de Montpellier (fol.\u00a0141\u00a0v\u00b0). \u2014 <span class=\"small-caps\">Gariel<\/span>, <i>Series<\/i>, t.\u00a0I, p.\u00a0199",
            "children": {
                "numero_note": "0"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<span class=\"small-caps\">Migne<\/span>, <i>Patr. lat.<\/i>, t.\u00a0CLXXXVIII, col.\u00a01534",
            "children": {
                "numero_note": "1"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<span class=\"small-caps\">Jaff\u00e9<\/span> et <span class=\"small-caps\">Wattenbach<\/span>, n\u00b0\u00a010335.",
            "children": {
                "numero_note": "2"
            }
        },
        {
            "type": "genre",
            "value": "Mandement (littere cum filo canapis)"
        }
    ]
}