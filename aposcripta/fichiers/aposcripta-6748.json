{
    "id": 147121,
    "name": "aposcripta-6748",
    "type": "acte",
    "url": "http:\/\/telma-chartes.irht.cnrs.fr\/aposcripta\/notice\/147121",
    "project_slug": "aposcripta",
    "project_label": "APOSCRIPTA \u2013 Lettres des papes",
    "user": "v_sentis",
    "last_update": "2020-10-19 16:39:26",
    "licence": "Attribution - Pas d\u2019Utilisation Commerciale - Partage dans les M\u00eames Conditions 3.0 France (CC BY-NC-SA 3.0 FR) ",
    "licence_url": "https:\/\/creativecommons.org\/licenses\/by-nc-sa\/3.0\/fr\/",
    "elements": [
        {
            "type": "pape",
            "value": "Jean XXII (1316-1334)"
        },
        {
            "type": "destinataire",
            "value": "Le roi de France Charles IV"
        },
        {
            "type": "date_libre",
            "value": "VIII idus aprilis anno ottavo"
        },
        {
            "type": "date",
            "value": "1324\/04\/06",
            "children": {
                "date_deb_jour": "06",
                "date_deb_mois": "04",
                "date_deb_annee": "1324"
            },
            "range": {
                "date_apres": 13240406,
                "date_avant": 13240406
            }
        },
        {
            "type": "date_lieu",
            "value": "Avignon"
        },
        {
            "type": "genre",
            "value": "Lettre, g\u00e9n\u00e9ral (apr\u00e8s 1198, littere cum filo canapis)"
        },
        {
            "type": "analyse",
            "value": "Le pape engage le roi de France \u00e0 ne pas \u00ab suivre deux li\u00e8vres \u00e0 la fois \u00bb, et \u00e0 ne pas se laisser engager dans un trop grand nombre d'affaires. Il l'exhorte en particulier \u00e0 ne pas d\u00e9clarer la guerre au roi d'Angleterre pour trancher les difficult\u00e9s qui mettaient aux prises, en Gascogne, les repr\u00e9sentants des deux souverains, et venger l'incendie de la bastide de Saint-Sardos. Il termine en lui offrant son arbitrage et eu demandant une prompte r\u00e9ponse qui pourra servir de base \u00e0 de nouvelles n\u00e9gociations [Guerard].\nRegi Franciae, ut initium guerrae cum Eduardo, rege Angliae, habendae differat [Coulon].\n"
        },
        {
            "type": "transcription",
            "value": "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\nRegi Francie1. Volenti, fili carissime, aggredi ardua, expedit illa vitare provide per que posset ejus impediri propositum vel periculose differri, ne, si forsan ad utrumque intenderet, frustraretur altero seu utroque, juxta illud poeticum :  \u00ab Qui duos lepores una sectabatur hora Uno quandoque, quandoque carebit utroque2 \u00bb. Scimus autem celsitudinem regiam talia jamdudum concepisse prosequi que utique ardua, utilia et multum statui regio honorabilia sunt censendaa. Verum quia, sicut habet rumor implacidus, ad suscitanda dissidia inter te et carissimum in Christo filium nostrum Eduardum, regem Anglie illustrem, hostis pacis et caritatis emulus adeo laboravit quod jam in partibus Vasconie officiales tui et regis prefati ad congressus bellicos se disponunt, ex quibus profecto, ut de dispendiis rerum, stragibus corporum ac animarum periculis, que inde possent subsequi, teneamusb, premissorum negotiorum prosecutio difficultates posset periculosas suscipere vel totaliter impediri, providentie regie sano consilio suademus ut sic caute guerre predicte initia differat quod vie per quam juri regio provideri posset commodius non obsistat. Et ecce, fili carissime, quod nos regi prefato super hoc scribimus ne tam enormem injuriam, qualis est tuo culmini in combustione bastite Sancti Sacerdotis et aliis commissis inibi irrogata, velit defendere, nec te in executione justitie impedire ; sed si juri suo per ea que circa corrigendam dictam injuriam fecisti hactenus vel feceris in futurum credat in aliquo derogari, illa coram te per semitas justitie tractatusque amicabiles prosequatur ; ad quos obtinendum eidem nos paratos offerimus apud tuam magnificentiam diligentiam sedulam adhibere. Ceterum petimus ut intentionem in hac parte tuam per tuas responsales litteras, celeritate qua convenit, nobis scribas, ut juxta tenorem responsionis hujusmodi quod expedit dicto negotio per oportunos nuntios vel alias prosequamur. Datum Avinione VIII idus aprilis anno VIII.\n"
        },
        {
            "type": "sources_manuscrites",
            "value": "R. CIT\u00c9 DU VATICAN,  Archivio segreto Vaticano, <i>Registra Vaticana<\/i> 112, pars secunda, fol. 21v, c. 605.",
            "children": {
                "type_source": "Registre de la chancellerie apostolique"
            }
        },
        {
            "type": "edition",
            "value": "Ici reprise de Louis Gu\u00e9rard, <i>Documents pontificaux sur la Gascogne d'apr\u00e8s les archives du Vatican. Pontificat de Jean XXII<\/i>, Paris, Auch : Honor\u00e9 Champion, L\u00e9once Cocharaux, 1896-1903, 2 vol. <a target=\"_blank\" href=\" http:\/\/gallica.bnf.fr\/ark:\/12148\/bpt6k208777t.r=documents%20pontificaux%20gascogne%20jean%20gu%C3%A9rard?rk=128756;0\"> [en ligne]<\/a>, t. II, n. 302, p. 128-129."
        },
        {
            "type": "edition_autre",
            "value": "Auguste Coulon, Suzanne Cl\u00e9mencet, <i>Jean XXII : lettres secr\u00e8tes et curiales relatives \u00e0 la France<\/i>, Paris, 1906-1973, n. 2008 <a target=\"_blank\" href=\" https:\/\/archive.org\/details\/lettressecrtes02joha\/page\/242\"> [en ligne]<\/a>. "
        },
        {
            "type": "notes_historiques",
            "value": "Le d\u00e9but de cette lettre donne une id\u00e9e de la familiarit\u00e9 de ton que l'on rencontre souvent dans la correspondance de Jean XXII avec la cour de France. Peut-\u00eatre les paroles du pape renferment-elles une allusion aux projets de Charles le Bel qui, vers cette m\u00eame \u00e9poque, visait \u00e0 se faire proclamer empereur (Paul Fournier, <i>Le royaume d'Arles et de Vienne<\/i>, Paris, 1891, p. 388). Quant \u00e0 la querelle de la France et de l'Angleterre, elle \u00e9tait entr\u00e9e dans une p\u00e9riode plus aigu\u00eb que jamais. \u00c9douard II n'avait pas encore pr\u00eat\u00e9 hommage au roi de France pour le duch\u00e9 d'Aquitaine, et, d'autre part, les incidents de fronti\u00e8res dont Jean XXII parle ici (n. 302) avaient d\u00e9j\u00e0 mis aux prises les officiers anglais et les officiers fran\u00e7ais. Le sire de Montpezat, vassal du roi d'Angleterre, avait construit la bastide de Saint-Sardos (arrondissement d'Agen, canton de Prayssas) sur un territoire contest\u00e9 par les Fran\u00e7ais. Ceux-ci y avaient alors mis garnison, mais ils en avaient \u00e9t\u00e9 chass\u00e9s et la bastide avait \u00e9t\u00e9 br\u00fbl\u00e9e, d'apr\u00e8s une lettre de Jean XXII, au mois d'octobre 1323 (Reg. Vat. 112, ep. 755). La moiti\u00e9 de l'ann\u00e9e 1324 se passa en n\u00e9gociations auxquelles le pape, quoi qu'on en ait dit, prit une large part (<i>Hist. de Languedoc<\/i>, IX, 433). On ne publie ici que deux documents sur cette affaire qui n'int\u00e9ressa qu'une petite partie des territoires compris dans les dioc\u00e8ses de la province eccl\u00e9siastique d'Auch (n. 302 et 308). Rappelons simplement que, le 12 mars 1324, Jean XXII avait \u00e9crit au roi d'Angleterre pour l'exhorter \u00e0 pr\u00eater au roi de France l'hommage d\u00fb \u00e0 raison du duch\u00e9 d'Aquitaine (Reg. Vat. 112, ep. 754). Le 6 avril, exhortations au roi de France et avis dont on lit ici le texte (n. 302). Le m\u00eame jour, le pape \u00e9crit au roi d'Angleterre et l'exhorte \u00e0 ne pas engager la guerre pour soutenir les exc\u00e8s commis par ses officiers dans la bastide de Saint-Sardos. Dans le cas, dit Jean XXII, o\u00f9 la cour de France aurait entam\u00e9 des proc\u00e9dures l\u00e9sant les droits du roi d'Angleterre, celui-ci pouvait en demander la r\u00e9vocation, et le pape se d\u00e9clarait pr\u00eat \u00e0 appuyer sa requ\u00eate (voir, pour la suite, le n. 308). ",
            "children": {
                "numero_note": "1"
            }
        },
        {
            "type": "apparat_critique",
            "value": "<I>sencenda<\/i> ms.",
            "children": {
                "numero_note": "a"
            }
        },
        {
            "type": "apparat_critique",
            "value": "<i>Le sens demande <\/i>taceamus <i>ou <\/i>silentium teneamus<i> Gu\u00e9rard ; Coulon corrige en <\/i>taceamus. ",
            "children": {
                "numero_note": "b"
            }
        }
    ]
}