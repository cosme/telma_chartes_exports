{
    "id": 148210,
    "name": "aposcripta-7826",
    "type": "acte",
    "url": "http:\/\/telma-chartes.irht.cnrs.fr\/aposcripta\/notice\/148210",
    "project_slug": "aposcripta",
    "project_label": "APOSCRIPTA \u2013 Lettres des papes",
    "user": "l_valliere",
    "last_update": "2020-10-19 16:39:42",
    "licence": "Attribution - Pas d\u2019Utilisation Commerciale - Partage dans les M\u00eames Conditions 3.0 France (CC BY-NC-SA 3.0 FR) ",
    "licence_url": "https:\/\/creativecommons.org\/licenses\/by-nc-sa\/3.0\/fr\/",
    "elements": [
        {
            "type": "pape",
            "value": "Cl\u00e9ment IV (1265-1268)"
        },
        {
            "type": "destinataire",
            "value": "Le potestat, le capitaine, le Conseil et les citoyens de P\u00e9rouse"
        },
        {
            "type": "date_libre",
            "value": "XVIII ou XVII kalendas julii, anno quarto"
        },
        {
            "type": "date",
            "value": "1268\/06\/14",
            "children": {
                "date_deb_annee": "1268",
                "date_deb_mois": "06",
                "date_deb_jour": "14"
            },
            "range": {
                "date_apres": 12680614,
                "date_avant": 12680614
            }
        },
        {
            "type": "date_lieu",
            "value": "Viterbe"
        },
        {
            "type": "genre",
            "value": "Lettre, g\u00e9n\u00e9ral (apr\u00e8s 1198, littere cum filo canapis)"
        },
        {
            "type": "analyse",
            "value": "Rogat eos, ut militiam sibi mittant, cum Corradinus, e Tuscia ad Urbem proficiscens, juxta Viterbium transitum sit facturus; promittens se eos extra contractam non missurum [Jordan].\nClemens IV. an Podest\u00e0, Capitano del Popolo, Rat und B\u00fcrger von Perugia: ruft eine Abordnung zu sich, die ihm mit ihrem Rat zur Seite zu stehen solle, wenn Konradin (von Schwaben) auf seinem Marsch nach Rom an Viterbo vorbeiziehen werde [Thumser]."
        },
        {
            "type": "sources_manuscrites",
            "value": "R<sup>1<\/sup>. CIT\u00c9 DU VATICAN, Archivio segreto Vaticano, <i>Registra Vaticana<\/i> 30, fol. 83.",
            "children": {
                "type_source": "Formulaire, collection de lettres m\u00e9di\u00e9vale"
            }
        },
        {
            "type": "sources_manuscrites",
            "value": "R<sup>2<\/sup>. CIT\u00c9 DU VATICAN, Archivio segreto Vaticano, <i>Registra Vaticana<\/i> 33, ep. 506, fol. 81.",
            "children": {
                "type_source": "Formulaire, collection de lettres m\u00e9di\u00e9vale"
            }
        },
        {
            "type": "sources_manuscrites",
            "value": "R<sup>3<\/sup>. CIT\u00c9 DU VATICAN, Archivio segreto Vaticano, <i>Registra Vaticana<\/i> 34, fol. 127.",
            "children": {
                "type_source": "Formulaire, collection de lettres m\u00e9di\u00e9vale"
            }
        },
        {
            "type": "sources_manuscrites",
            "value": "R<sup>4<\/sup>. CIT\u00c9 DU VATICAN, Archivio segreto Vaticano, <i>Registra Vaticana<\/i> 35, ep. 513, fol. 93 (<i>XVII kal. jul.<\/i>).",
            "children": {
                "type_source": "Formulaire, collection de lettres m\u00e9di\u00e9vale"
            }
        },
        {
            "type": "edition",
            "value": "Ici reprise de Mathias Thumser, <i>Die Briefe Papst Clemens\u2019 IV. (1265-1268)<\/i>, Vorl\u00e4ufige Edition, MGH, 2015, n. 514."
        },
        {
            "type": "regeste",
            "value": "Potthast, <i>Regesta pontificum Romanorum<\/i>, n. 20391 <a target=\"_blank\" href=\"https:\/\/archive.org\/stream\/RegestaPontificumRomanorum\/ph#page\/n1655\/mode\/2up\"> [en ligne]<\/a>."
        },
        {
            "type": "bibliographie_generale",
            "value": "Mathias Thumser, \u00ab Zur \u00dcberlieferungsgeschichte der Briefe Clemens'IV. (1265-1268) \u00bb, <i>Deutsches Archiv f\u00fcr Erforschung des Mittelalters<\/i>, 51\/1, 1995, p. 115-168."
        },
        {
            "type": "transcription",
            "value": "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\nDilectis filiis potestati, capitaneo, consilio et civibus Perusinis. Diversis diversa narrantibus, sed finaliter in hanc unam sententiam concordantibus, quod Corradinus a viris perfidis inductus in Tusciam et exinde traducendus ad Urbem iuxta nos transitum est facturus, licet ad eius impediendum transitum nequeamus habere militiam numerosam, placuit tamen nobis de nostris et ecclesie fidelioribus aliquos habere nobiscum, quorum gratissimo ministerio ceteros fideles ecclesie roboremus. Et ideo vos vocavimus, quorum fidem pre ceteris et modernis et antiquis temporibus novimus approbatam, nec extra contratam mittere vos intendimus, sed vestro potius consilio dirigi et presentia recreari. Datum Viterbii, XVIII\/XVII kalendas iulii, anno IIII.\n"
        },
        {
            "type": "edition_autre",
            "value": "Edmond Mart\u00e8ne, Ursin Durand, <i>Thesaurus novus anecdotorum<\/i>, Paris, 1717, II, n. 663, col. 609 (<i>id. junii<\/i>) <a target=\"_blank\" href=\"https:\/\/archive.org\/stream\/ThesaurusNovusAnecdotorum2\/Thesaurus_novus_anecdotorum_2#page\/n339\/mode\/2up\"> [en ligne]<\/a>."
        },
        {
            "type": "regeste",
            "value": "\u00c9douard Jordan, <i>Les registres de Cl\u00e9ment IV (1265-1268)<\/i>, Paris, 1893-1945, n. 1383, d\u2019apr\u00e8s R<sup>1<\/sup>, R<sup>2<\/sup>, R<sup>3<\/sup> et R<sup>4<\/sup>."
        },
        {
            "type": "date",
            "value": "1268\/06\/15",
            "children": {
                "date_deb_annee": "1268",
                "date_deb_mois": "06",
                "date_deb_jour": "15"
            },
            "range": {
                "date_apres": 12680615,
                "date_avant": 12680615
            }
        }
    ]
}