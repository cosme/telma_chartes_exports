{
    "id": 24130,
    "name": "aposcripta-1282",
    "type": "acte",
    "url": "http:\/\/telma-chartes.irht.cnrs.fr\/aposcripta\/notice\/24130",
    "project_slug": "aposcripta",
    "project_label": "APOSCRIPTA \u2013 Lettres des papes",
    "user": "lettrespontificalesadmin",
    "last_update": "2020-10-19 16:27:23",
    "licence": "Attribution - Pas d\u2019Utilisation Commerciale - Partage dans les M\u00eames Conditions 3.0 France (CC BY-NC-SA 3.0 FR) ",
    "licence_url": "https:\/\/creativecommons.org\/licenses\/by-nc-sa\/3.0\/fr\/",
    "elements": [
        {
            "type": "pape",
            "value": "Alexandre III (1159-1181)"
        },
        {
            "type": "date_lieu",
            "value": "n.c."
        },
        {
            "type": "date_libre",
            "value": "7\u00a0juin 1168-1169"
        },
        {
            "type": "date",
            "value": "apr\u00e8s 1168\/06\/07 - avant 1169\/06\/07",
            "children": {
                "date_deb_annee": "1168",
                "date_deb_mois": "06",
                "date_deb_jour": "07",
                "date_deb_type": "apr\u00e8s",
                "date_fin_annee": "1169",
                "date_fin_mois": "06",
                "date_fin_jour": "07",
                "date_fin_type": "avant"
            },
            "range": {
                "date_apres": 11680607,
                "date_avant": 11690607
            }
        },
        {
            "type": "analyse",
            "value": "Alexandre\u00a0III d\u00e9fend aux chanoines de Maguelone les fonctions d\u2019avocat en mati\u00e8re civile."
        },
        {
            "type": "transcription",
            "value": "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\nAlexandera episcopus, servus servorum Dei, dilectis filiis preposito et canonicis Magalonensibusb, salutem et apostolicam benedictionem.Ad audientiamc nostram semel iterumd pervenisse noscatis, quod quidam vestrum, peritiame legum habentes, in causis secularibus, contra prohibitionemf nostram, et decretum etiamg quod inde jam pridem edidimus, advocationeh fungi presumunt, nec ab eisi, sepe commoniti, volunt aliqua ratione cessare.Unde, quoniamj id omnino indignum et a religione vestrak penitus alienum existit, universitati vestre per apostolica scripta mandamus, quatenus a presumptione hujusmodi memoratos fratres vestros desistere compellatis, nec ipsos, preterquam in causis ecclesiasticis, patrocinium suum cuiquam permittatis prestare. Quod si illos a talibus nolueritis cohibere, volumus ut venerabilis frater noster, episcopus vester, penam super hoc in decreto nostro expressam eis, omni contradictione et appellatione cessante, infligat.Datum Beneventi, vii idus junii.\n"
        },
        {
            "type": "edition",
            "value": "Julien Rouquette, Augustin Villemagne, <i>Bullaire de l'\u00c9glise de Maguelone<\/i>, Paris, Montpellier : Picard, Valat, 1911-1914, I, n. 94-LXIV, p. 152-154"
        },
        {
            "type": "remarques",
            "value": "Avec cette bulle nous terminons la s\u00e9rie de celles envoy\u00e9es de B\u00e9n\u00e9vent par Alexandre\u00a0III soit au chapitre soit \u00e0 l\u2019\u00e9v\u00eaque. Nous les avons ins\u00e9r\u00e9es dans l\u2019ordre que nous avons trouv\u00e9 dans <span class=\"small-caps\">Jaff\u00e9<\/span> et <span class=\"small-caps\">Wattenbach<\/span>, et avec la date fournie par ces auteurs et par <span class=\"small-caps\">Germain<\/span>. Il est tr\u00e8s difficile, nous l\u2019avouons, de trouver, m\u00eame apr\u00e8s une lecture attentive, une suite chronologique \u00e0 ces diverses pi\u00e8ces. Il nous semble cependant que, \u00e0 l\u2019aide d\u2019autres documents, on pourrait, au moins pour les plus importantes, pr\u00e9ciser la date, et jeter un peu de jour sur cette partie de notre histoire dioc\u00e9saine.\n\nAlexandre\u00a0III arriva \u00e0 B\u00e9n\u00e9vent vers la fin ao\u00fbt 1167 et y s\u00e9journa jusqu\u2019\u00e0 la fin f\u00e9vrier 1170. Or deux actes du <i>Cartulaire de Maguelone<\/i> pourraient nous servir pour dater quelques-unes de ces bulles\u00a0: le premier est de juin 1168 (Cf. <i>Cart. de Maguelone<\/i>, reg. D. fol.\u00a0301, et reg. C. fol.\u00a0130)\u00a0; le second est de d\u00e9cembre 1168 et dat\u00e9 de B\u00e9n\u00e9vent (<i>Ibid.<\/i>, reg. F., fol.\u00a0140).\n\nPar le premier (juin 1168), le pr\u00e9v\u00f4t Fulcrand abandonne \u00e0 l\u2019\u00e9v\u00eaque et \u00e0 ses successeurs les 650 sols que l\u2019\u00e9v\u00eaque Raimond devait \u00e0 la communaut\u00e9, et aussi les 1.000 sols que le chanoine ouvrier avait pr\u00eat\u00e9s \u00e0 Jean de Montlaur, \u00e9v\u00eaque, et fonde m\u00eame en faveur du pr\u00e9lat un service anniversaire apr\u00e8s sa mort.\n\nLe second est plus important. Nous citerons les passages qui peuvent nous servir. L\u2019accord fut conclu \u00e0 B\u00e9n\u00e9vent en d\u00e9cembre 1168\u00a0; par cons\u00e9quent le chapitre et l\u2019\u00e9v\u00eaque avaient envoy\u00e9 des d\u00e9l\u00e9gu\u00e9s aupr\u00e8s du Pape, et nous avons vu que dans quelques bulles, Alexandre\u00a0III y fait allusion.\n\nDe cet accord nous extrayons les trois passages suivants, mentionn\u00e9s dans les bulles\u00a0: 1\u00b0\u00a0l\u2019\u00e9v\u00eaque donnera, dans l\u2019espace de deux ans, au chanoine ouvrier les vingt marcs d\u2019argent qu\u2019il doit \u00e0 l\u2019\u0153uvre de Maguelone\u00a0; 2\u00b0\u00a0sa vie durant, l\u2019\u00e9v\u00eaque pourra garder son chapelain s\u00e9culier, qui restera \u00e0 la charge de la communaut\u00e9\u00a0; 3\u00b0\u00a0les \u00e9cuyers, coureurs ou autres familiers de l\u2019\u00e9v\u00eaque ne pourront \u00eatre admis \u00e0 sa table, par cons\u00e9quent, ne seront pas \u00e0 la charge de la communaut\u00e9.\n\nIl est \u00e9vident d\u00e8s lors que la bulle <i>Cum Ecclesia tibi commissa<\/i> est du 16\u00a0mai 1168, puisque c\u2019est dans cette bulle que le Pape reproche \u00e0 l\u2019\u00e9v\u00eaque d\u2019avoir un chapelain s\u00e9culier, et des \u00e9cuyers et des coureurs qui sont \u00e0 la charge de la communaut\u00e9.\n\nC\u2019est aussi au 16\u00a0mai 1168 qu\u2019il faut fixer la lettre adress\u00e9e aux \u00e9v\u00eaques de Nimes et de B\u00e9ziers et \u00e0 l\u2019abb\u00e9 de Saint-Gilles\u00a0: <i>Causam que inter venerabilem<\/i>, puisque dans la bulle \u00e0 l\u2019\u00e9v\u00eaque de Maguelone, dat\u00e9e de ce jour, le Pape y fait allusion.\n\nC\u2019est enfin au 16\u00a0mai 1168 qu\u2019il faut fixer la bulle <i>Devotionis et fidei<\/i>, puisque dans cette bulle le Pape reproche \u00e0 l\u2019\u00e9v\u00eaque de devoir encore vingt marcs donn\u00e9s par son pr\u00e9d\u00e9cesseur au chapitre et lui ordonne de les payer\u00a0; or ces vingt marcs figurent dans la transaction de d\u00e9cembre 1168.\n\nIl nous semble que la date assign\u00e9e \u00e0 ces trois bulles est certaine. Un passage de la derni\u00e8re\u00a0: <i>Devotionis et fidei<\/i>, pourrait nous guider pour r\u00e9tablir d\u2019autres dates. Tout d\u2019abord le Pape proteste de son affection pour l\u2019\u00e9v\u00eaque et du d\u00e9sir de sauvegarder sa dignit\u00e9\u00a0: le Pape ne ferait-il pas ici allusion \u00e0 deux bulles, toutes deux du mois de d\u00e9cembre, l\u2019une par laquelle il lui donne toute juridiction sur l\u2019\u00e9glise et l\u2019h\u00f4pital du bois de Galtier\u00a0: <i>Ad conservanda et manutenenda<\/i>\u00a0; l\u2019autre, par laquelle il ordonne que toutes les causes contre les chanoines seront port\u00e9es au tribunal de l\u2019\u00e9v\u00eaque\u00a0: <i>Fratres et coepiscopos\u00a0?<\/i> D\u00e8s lors, ces deux bulles seraient du 11 et du 14\u00a0d\u00e9cembre 1167. Ce n\u2019est qu\u2019une hypoth\u00e8se, mais elle nous para\u00eet bien probable.\n\nDans la bulle <i>Devotionis et fidei<\/i>, Alexandre\u00a0III avoue \u00e0 l\u2019\u00e9v\u00eaque avoir re\u00e7u une lettre de Louis\u00a0VII en sa faveur. Jean de Montlaur devait avoir des motifs assez graves pour recourir \u00e0 la protection royale. Ces motifs, nous les trouvons dans les privil\u00e8ges accord\u00e9s au pr\u00e9v\u00f4t et au chapitre soit par la bulle <i>Illius sincerissime devotionis<\/i>, du 12\u00a0f\u00e9vrier 1166, par laquelle l\u2019\u00e9v\u00eaque ne pouvait excommunier le pr\u00e9v\u00f4t, soit par la reconnaissance de la pr\u00e9v\u00f4t\u00e9.\n\nDans quatre bulles, Alexandre\u00a0III fait allusion aux envoy\u00e9s de l\u2019\u00e9v\u00eaque et du chapitre. Nous savons, d\u2019apr\u00e8s le <i>Cartulaire de Maguelone<\/i>, ainsi que nous l\u2019avons dit au commencement de cette note, que ces envoy\u00e9s conclurent un accord \u00e0 B\u00e9n\u00e9vent en d\u00e9cembre 1168\u00a0; les quatre bulles sont donc contemporaines de ce fait ou post\u00e9rieures.\n\nLes deux premi\u00e8res sont les deux bulles <i>Ex injuncte nobis officio<\/i>, adress\u00e9es l\u2019une au pr\u00e9v\u00f4t, l\u2019autre \u00e0 l\u2019\u00e9v\u00eaque. Toutes deux doivent \u00eatre dat\u00e9es de d\u00e9cembre 1168. Elles furent certainement donn\u00e9es aux envoy\u00e9s de l\u2019\u00e9v\u00eaque et du pr\u00e9v\u00f4t, ainsi que cela ressort de ce passage qui se trouve dans les deux pi\u00e8ces pontificales\u00a0: <i>quum ad propria, ducente Domino, redieritis<\/i>.\n\nLa troisi\u00e8me, <i>Cum nuntii venerabilis<\/i>, fut donn\u00e9e peu de jours apr\u00e8s\u00a0: elle serait donc du 13\u00a0d\u00e9cembre 1168.\n\nLa quatri\u00e8me, <i>Cum venerabilis fratris<\/i>, est certainement post\u00e9rieure \u00e0 la pr\u00e9c\u00e9dente puisqu\u2019elle l\u2019explique touchant un point important\u00a0: la pr\u00e9sence de l\u2019\u00e9v\u00eaque \u00e0 la nomination des prieurs. Elle est donc du 16\u00a0mai 1169.\n\nIl reste encore quatre bulles\u00a0: deux se rapportant \u00e0 des monast\u00e8res de femmes, et deux int\u00e9ressant le chapitre. Nous avouons que nous ne connaissons aucun motif pour leur assigner une date plus pr\u00e9cise que celle que nous leur donnons avec <span class=\"small-caps\">Germain<\/span> et <span class=\"small-caps\">Jaff\u00e9<\/span>.\n\n\u00c9tablissons d\u2019apr\u00e8s ces donn\u00e9es la suite chronologique de ces bulles, faisant suivre d\u2019un point d\u2019interrogation les deux qui nous paraissent douteuses.\n\n11\u00a0d\u00e9cembre 1167(?), bulle \u00e0 l\u2019\u00e9v\u00eaque, <i>Fratres et coepiscopos<\/i>.\n\n14\u00a0d\u00e9cembre 1167(?), bulle \u00e0 l\u2019\u00e9v\u00eaque, <i>Ad conservanda et manutenenda<\/i>.\n\n16\u00a0mai 1168, bulle \u00e0 l\u2019\u00e9v\u00eaque, <i>Cum Ecclesia tibi commissa<\/i>.\n\n16\u00a0mai 1168, bulle aux \u00e9v\u00eaques de Nimes et de B\u00e9ziers, etc. <i>Causam que inter<\/i>.\n\n16\u00a0mai 1168, bulle \u00e0 l\u2019\u00e9v\u00eaque, <i>Devotionis et fidei<\/i>.\n\n7\u00a0d\u00e9cembre 1168, bulle au pr\u00e9v\u00f4t, <i>Ex injuncte nobis officio<\/i>.\n\n10\u00a0d\u00e9cembre 1168, bulle \u00e0 l\u2019\u00e9v\u00eaque, <i>Ex injuncte nobis officio<\/i>.\n\n13\u00a0d\u00e9cembre 1168, bulle au pr\u00e9v\u00f4t, <i>Cum nuntii venerabilis<\/i>.\n\n16\u00a0mai 1169, bulle au pr\u00e9v\u00f4t, <i>Cum venerabilis fratris<\/i>.\n\n  Si on lit ces bulles dans cet ordre il nous semble que les \u00e9v\u00e9nements s\u2019encha\u00eenent. C\u2019est la chronologie que nous proposons et qui nous para\u00eet admissible. Quelle que soit d\u2019ailleurs l\u2019opinion que l\u2019on adopte, il n\u2019en sera pas moins vrai que l\u2019accord conclu \u00e0 B\u00e9n\u00e9vent en pr\u00e9sence du Pape, en d\u00e9cembre 1168, ne doive servir de guide dans ce diff\u00e9rend entre Jean de Montlaur et Fulcrand. Nous croyons que si <span class=\"small-caps\">Germain<\/span> l\u2019avait connu quand il composa son \u00e9tude sur <i>Maguelone sous ses \u00e9v\u00eaques<\/i>, comme il le connut plus tard, quand il \u00e9dita <span class=\"small-caps\">Arnaud de Verdale<\/span> (Pi\u00e8ces justificatives, p.\u00a0207), il aurait certainement profit\u00e9 de ce document pour introduire un peu d\u2019ordre et de clart\u00e9 dans cet imbroglio chronologique.\n\n[Rouquette-Villemagne]"
        },
        {
            "type": "apparat_critique",
            "value": "Cette bulle ne se trouve pas dans la seconde copie du <i>Bullaire<\/i>, que nous suivons toujours\u00a0; le folio a \u00e9t\u00e9 enlev\u00e9. Nous l\u2019avons collationn\u00e9e sur la premi\u00e8re copie.",
            "children": {
                "numero_note": "a"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Bull. et Cart.\u00a0: <i>Magalonen<\/i>, surmont\u00e9 de l\u2019abr\u00e9viation, que nous traduisons toujours par <i>Magalonensis<\/i>\u00a0; Germain\u00a0: <i>Magalone<\/i>.",
            "children": {
                "numero_note": "b"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Bull.\u00a0: <i>audienciam<\/i>\u00a0; Cart. et Germain\u00a0: <i>audientiam<\/i>.",
            "children": {
                "numero_note": "c"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Bull. et Cart.\u00a0: <i>iterum<\/i>\u00a0; Germain\u00a0: <i>iterum<\/i>[<i>que<\/i>].",
            "children": {
                "numero_note": "d"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Bull.\u00a0: <i>periciam<\/i>\u00a0; Cart. et Germain\u00a0: <i>peritiam<\/i>.",
            "children": {
                "numero_note": "e"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Bull.\u00a0: <i>prohibicionem<\/i>\u00a0; Cart. et Germain\u00a0: <i>prohibitionem<\/i>.",
            "children": {
                "numero_note": "f"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Bull. et Germain\u00a0: <i>etiam<\/i>\u00a0; Cart.\u00a0: <i>eciam<\/i>.",
            "children": {
                "numero_note": "g"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Bull.\u00a0: <i>advocacione<\/i>\u00a0; Cart. et Germain\u00a0: <i>advocatione<\/i>.",
            "children": {
                "numero_note": "h"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Bull. et Germain\u00a0: <i>eis<\/i>\u00a0; Cart.\u00a0: <i>his<\/i>.",
            "children": {
                "numero_note": "i"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Bull. et Cart.\u00a0: <i>qm<\/i> = <i>quoniam<\/i>\u00a0; Germain\u00a0: <i>quum<\/i>.",
            "children": {
                "numero_note": "j"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Bull.\u00a0: <i>vra<\/i> (= <i>vestra<\/i>) ou <i>nra<\/i> (= <i>nostra<\/i>)\u00a0: Cart.\u00a0: <i>vra<\/i>\u00a0; Germain\u00a0: <i>nostra<\/i>. N\u2019aurions-nous pas la le\u00e7on du <i>Cartulaire<\/i>, qui nous para\u00eet certaine, que la lecture de <span class=\"small-caps\">Germain<\/span> serait \u00e0 rejeter \u00e0 cause du contexte.",
            "children": {
                "numero_note": "k"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<i>Cart. de Maguelone<\/i>, reg.\u00a0B, fol.\u00a0270\u00a0v\u00b0",
            "children": {
                "numero_note": "0"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<i>Bull. de Maguelone<\/i>, fol.\u00a08\u00a0v\u00b0",
            "children": {
                "numero_note": "1"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<span class=\"small-caps\">Germain<\/span>, <i>Maguelone sous ses \u00e9v\u00eaques<\/i>, p.\u00a0182",
            "children": {
                "numero_note": "2"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<span class=\"small-caps\">Jaff\u00e9<\/span> et <span class=\"small-caps\">Wattenbach<\/span>, n\u00b0\u00a011549.",
            "children": {
                "numero_note": "3"
            }
        },
        {
            "type": "genre",
            "value": "Bulle"
        }
    ]
}