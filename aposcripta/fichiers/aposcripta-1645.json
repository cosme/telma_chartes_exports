{
    "id": 24494,
    "name": "aposcripta-1645",
    "type": "acte",
    "url": "http:\/\/telma-chartes.irht.cnrs.fr\/aposcripta\/notice\/24494",
    "project_slug": "aposcripta",
    "project_label": "APOSCRIPTA \u2013 Lettres des papes",
    "user": "lettrespontificalesadmin",
    "last_update": "2020-10-19 16:27:30",
    "licence": "Attribution - Pas d\u2019Utilisation Commerciale - Partage dans les M\u00eames Conditions 3.0 France (CC BY-NC-SA 3.0 FR) ",
    "licence_url": "https:\/\/creativecommons.org\/licenses\/by-nc-sa\/3.0\/fr\/",
    "elements": [
        {
            "type": "pape",
            "value": "Alexandre IV (1254-1261)"
        },
        {
            "type": "date_lieu",
            "value": "n.c."
        },
        {
            "type": "date_libre",
            "value": "4\u00a0janvier 1257"
        },
        {
            "type": "date",
            "value": " 1257\/01\/04",
            "children": {
                "date_deb_annee": "1257",
                "date_deb_mois": "01",
                "date_deb_jour": "04"
            },
            "range": {
                "date_apres": 12570104,
                "date_avant": 12570104
            }
        },
        {
            "type": "analyse",
            "value": "Alexandre\u00a0IV \u00e9crit au sacriste de Nimes, pour qu\u2019il r\u00e9tablisse la paix entre les habitants de Marseille et ceux de Montpellier."
        },
        {
            "type": "transcription",
            "value": "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\nAlexander episcopus, servus servorum Dei, dilecto filio sacriste Nemausensi, salutem et apostolicam benedictionem.Quia inter sollicitudinis apostolice curas, causarum silere jurgia et dissensionum sedari materias cupimus, accedit ad non modicum gaudium votis nostris, si quando litigantium partes ad pacis et concordie bonum deveniunt, et jurgia et dissensiones hujusmodi amicabiliter sopiuntur\u00a0: et voluntati nostre omnino extitit contrarium, cum per ausum temerarium cujuspiam talis pax et concordia infringuntur, maxime quando inter devotos Ecclesie per predictum bonum quietis commodum fuerit procuratum.Ex parte siquidem dilectorum filiorum consulum et universitatis hominum Montispesulani, Magalonensis diocesis, coram nobis exposita petitio continebat quod inter ipsos, ex una parte, et universitatem Marsiliensem super diversis articulis, ex altera, materia contentionis exorta, tandem contentiones hujusmodi fuerunt per pacem et concordiam terminate, de pace et concordia hujusmodi observandis, et quod altera pars alteram non offenderet, ab ipsis partibus prestito corporaliter juramento.Verum universitas Marsiliensis predicta, proprie salutis immemores, predicti juramenti religione contempta, contra pacem et concordiam hujusmodi et juramentum super hoc ab ea prestitum venire temere non formidans, prefatis consulibus et universitati Montispesulani dampna gravia et injurias irrogarunt\u00a0: super quo iidem consules et universitas Montispesulani petierunt sibi per Sedem Apostolicam provideri.Quia vero nostra interest corripere peccatorem, ut ipsum revocemus a vitio ad virtutem, et juramentum, non ut esset nequitie aut doli materia, sed ut foret firmamentum fidei, est inventum, discretioni tue per apostolica scripta mandamus quatinus, si tibi constiterit ita esse, universitatem Marsiliensem predictam quod hujusmodi juramentum observent, ac eisdem consulibus et universitati Montispesulani de dampnis et injuriis supradictis plenariam satisfactionem impendant, monitione premissa, per censuram ecclesiasticam, sicut justum fuerit, appellatione remota, compellas.Datam Laterani, ii nonas januarii, pontificatus nostri anno tertio.\n"
        },
        {
            "type": "edition",
            "value": "Julien Rouquette, Augustin Villemagne, <i>Bullaire de l'\u00c9glise de Maguelone<\/i>, Paris, Montpellier : Picard, Valat, 1911-1914, II, n. 469-CCCXXXV, p. 353-354"
        },
        {
            "type": "remarques",
            "value": "L\u2019int\u00e9r\u00eat qu\u2019offre cette bulle m\u00e9rite d\u2019arr\u00eater notre attention. Nous voyons dans le fait non seulement la rivalit\u00e9 commerciale de Marseille et de Montpellier, mais encore un nouveau pas, et bien grand, de saint Louis pour s\u2019introduire dans cette derni\u00e8re ville, au c\u0153ur m\u00eame du comt\u00e9 de Melgueil. A cette \u00e9poque, Marseille d\u00e9pendait de Charles d\u2019Anjou. Saint Louis r\u00e9ussit, gr\u00e2ce aux rivalit\u00e9s entre les deux grandes places de commerce du Midi de la France, \u00e0 entrer dans Montpellier. Reprenons les faits d\u2019assez haut.\n\n  Nous avons d\u00e9j\u00e0 relat\u00e9 l\u2019importance du commerce de Montpellier au <span class=\"small-caps\">xiii<\/span>  <sup>e<\/sup> si\u00e8cle\u00a0: ses vaisseaux sillonnent la M\u00e9diterran\u00e9e, et, cela ressort du moins d\u2019une des clauses du trait\u00e9 de paix, notre ville devait \u00eatre sup\u00e9rieure \u00e0 celle de Marseille, puisqu\u2019elle dut payer 60.000 sous royaux (voir ci-dessous) pour compenser les pertes inflig\u00e9es \u00e0 la cit\u00e9 rivale.\n\nCe fut dans les comptoirs du Levant, \u00e0 Saint-Jean-d\u2019Acre, qu\u2019\u00e9clata le conflit, et \u00e0 une date que nous ne pouvons pr\u00e9ciser\u00a0: nous le concluons du trait\u00e9 de paix fait entre les deux villes, le 10\u00a0mai 1249 (Cf. <span class=\"small-caps\">Germain<\/span>, <i>Hist. de la commune de Montpellier<\/i>, t.\u00a0II, pp.\u00a0465 et sq.). Mais d\u00e9j\u00e0, en d\u00e9cembre 1229, un trait\u00e9 avait \u00e9t\u00e9 conclu entre elles pour une dur\u00e9e de cinq ans (Cf. <span class=\"small-caps\">Germain<\/span>, <i>ibid.<\/i>, pp.\u00a0457 et sq.). La paix de 1249 ne dut pas \u00eatre bien \u00e9tablie\u00a0: un nouveau trait\u00e9 fut conclu, le 19\u00a0d\u00e9cembre 1254 (Cf. <span class=\"small-caps\">Germain<\/span>, <i>ibid.<\/i>, pp.\u00a0477 et sq.). Il semble bien que, jusqu\u2019ici, tout se soit born\u00e9 \u00e0 quelques escarmouches entre les commer\u00e7ants des deux cit\u00e9s rivales. Apr\u00e8s le trait\u00e9 de 1254 survint une grande guerre\u00a0: <i>guerra magna exorta esset... propter aliqua, qu\u00e6 facta fuisse dicebantur in partibus transmarinis et cismarinis<\/i>, lisons-nous dans le trait\u00e9 de 1257 (Cf. <span class=\"small-caps\">Vaissete<\/span>, <i>Hist. g\u00e9n. de Languedoc<\/i>, t.\u00a0VIII, col.\u00a01413). Quand Charles, comte de Provence, eut pris la ville de Marseille, il fit ses efforts pour r\u00e9tablir la paix\u00a0: les arbitres ne purent s\u2019entendre. Les gens de Montpellier se disaient l\u00e9s\u00e9s\u00a0: ils firent appel \u00e0 Alexandre\u00a0IV, dont nous reproduisons la r\u00e9ponse, et aussi \u00e0 saint Louis, qui ordonna, vers cette \u00e9poque, une enqu\u00eate sur les pr\u00e9judices que les Marseillais auraient pu commettre contre le commerce montpelli\u00e9rain dans le port d\u2019Aiguesmortes (Cf. <span class=\"small-caps\">Germain<\/span>, <i>Hist. du Commerce de Montpellier<\/i>, t.\u00a0I, p.\u00a0225). Charles d\u2019Anjou imposa sa m\u00e9diation, et la paix fut conclue \u00e0 Brignoles, le 9\u00a0juillet 1257. \u00ab\u00a0Par l\u2019un des articles du trait\u00e9, les habitants de Montpellier furent condamn\u00e9s \u00e0 payer soixante mille sols royaux \u00e0 ceux de Marseille pour les d\u00e9dommager des pertes qu\u2019ils leur avoient caus\u00e9es\u00a0; preuve que les premiers avoient \u00e9t\u00e9 sup\u00e9rieurs durant cette guerre.\u00a0\u00bb (<span class=\"small-caps\">Vaissete<\/span>, <i>Hist. g\u00e9n. de Languedoc<\/i>, t.\u00a0VI, p.\u00a0848).\n\n[Rouquette-Villemagne]"
        },
        {
            "type": "bibliographie_generale",
            "value": "Archives municipales de Montpellier, arm.\u00a0E, cass.\u00a04, n\u00b0\u00a02182 de l\u2019Inventaire <span class=\"small-caps\">Louvet<\/span>, publi\u00e9 par <span class=\"small-caps\">M.J. Berthel\u00e9<\/span>\u00a0; original sur parchemin\u00a0; sceau de plomb attach\u00e9 par des cordons de chanvre",
            "children": {
                "numero_note": "0"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<span class=\"small-caps\">Germain<\/span>, <i>Hist. du Commerce de Montpellier<\/i>, t.\u00a0I, p.\u00a0222\u00a0; <i>Hist. de la commune de Montpellier<\/i>, t.\u00a0II, p.\u00a062",
            "children": {
                "numero_note": "1"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<span class=\"small-caps\">F. Fabr\u00e8ge<\/span>, <i>Hist. de Maguelone<\/i>, t.\u00a0II, p.\u00a099",
            "children": {
                "numero_note": "2"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<span class=\"small-caps\">Vaissete<\/span>, <i>Hist. g\u00e9n. de Languedoc<\/i>, t.\u00a0VI, p.\u00a0845.",
            "children": {
                "numero_note": "3"
            }
        },
        {
            "type": "genre",
            "value": "Bulle"
        }
    ]
}