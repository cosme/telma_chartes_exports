{
    "id": 24435,
    "name": "aposcripta-1586",
    "type": "acte",
    "url": "http:\/\/telma-chartes.irht.cnrs.fr\/aposcripta\/notice\/24435",
    "project_slug": "aposcripta",
    "project_label": "APOSCRIPTA \u2013 Lettres des papes",
    "user": "lettrespontificalesadmin",
    "last_update": "2020-10-19 16:27:29",
    "licence": "Attribution - Pas d\u2019Utilisation Commerciale - Partage dans les M\u00eames Conditions 3.0 France (CC BY-NC-SA 3.0 FR) ",
    "licence_url": "https:\/\/creativecommons.org\/licenses\/by-nc-sa\/3.0\/fr\/",
    "elements": [
        {
            "type": "pape",
            "value": "Innocent IV (1243-1254)"
        },
        {
            "type": "date_lieu",
            "value": "Lyon"
        },
        {
            "type": "date_libre",
            "value": "8\u00a0juillet 1248"
        },
        {
            "type": "date",
            "value": " 1248\/07\/08",
            "children": {
                "date_deb_annee": "1248",
                "date_deb_mois": "07",
                "date_deb_jour": "08"
            },
            "range": {
                "date_apres": 12480708,
                "date_avant": 12480708
            }
        },
        {
            "type": "analyse",
            "value": "Innocent\u00a0IV approuve la permutation faite par l\u2019\u00e9v\u00eaque de l\u2019\u00e9glise de Sainte-Marie de Melgueil, d\u00e9pendant du chapitre, contre quelques \u00e9glises d\u00e9pendant de la mense \u00e9piscopale."
        },
        {
            "type": "transcription",
            "value": "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\nInnocentiusa episcopus, servus servorum Dei, venerabili fratri, episcopo Magalonensi, salutem et apostolicam benedictionem.Que provide peraguntur et in alterius prejudicium non redundant, tanto libentiusb roboramur apostolici muniminis firmitate, quanto potest ex eis major utilitas provenire.Sane, sicut nobis exponere curavisti, tu, diligenter attendens quod castrum Melgorii, quod comitatus Melgorii et Montisferrandi caput existit, tanto expedit studiosius et cautiusc custodirid, quanto per diligenteme custodiam totus habetur tutiorf comitatus, et minus metuit insidias plurimorum eum anhelantiumg, si se offerret commoditas occupare ecclesiam quandam, quam Magalonense capitulum in castro ipso habuit, menibus adherentem, in quarundam ecclesiarum tuarum concambium ab eodem capitulo recepistih, per quam poterit castrum ipsum, in quo non habebas mansionem accomodam, melius, cautiusi et commodius custodiri.Nos igitur, statum comitatus ejusdem semper in melius dirigi cupientes, tuis precibus inclinati, permutationemj hujusmodi gratam et ratam habentes, ipsam auctoritate apostolica confirmamus, et presentis scripti patrocinio communimus.Nulli ergo omnino hominum liceat hanc paginam nostre confirmationisk infringere, vel ei ausu temerario contraire. Si quis autem hoc attemptare presumpserit, indignationeml omnipotentis Dei et beatorum Petri et Pauli, apostolorum ejus, se noverit incursurum.Datum Lugduni, viii\u00b0 m idusn julii, pontificatus nostri anno sexto.\n"
        },
        {
            "type": "edition",
            "value": "Julien Rouquette, Augustin Villemagne, <i>Bullaire de l'\u00c9glise de Maguelone<\/i>, Paris, Montpellier : Picard, Valat, 1911-1914, II, n. 409-CCXCI, p. 271-272"
        },
        {
            "type": "remarques",
            "value": "  Date. \u2014 Nous suivons la date du <i>Bullaire<\/i> et du <i>Cartulaire<\/i> (fol.\u00a0166\u00a0v\u00b0), n\u2019ayant pour cela d\u2019autre raison que de l\u2019avoir trouv\u00e9e identique dans deux manuscrits. De l\u00e0 la diff\u00e9rence de date que l\u2019on constatera entre nous et <span class=\"small-caps\">Germain<\/span> et <span class=\"small-caps\">M.F. Fabr\u00e8ge<\/span>.\n\nSi la mission r\u00e9formatrice que Innocent\u00a0IV aurait confi\u00e9e \u00e0 Rainier est une assertion purement gratuite de <span class=\"small-caps\">Germain<\/span>, comme nous l\u2019avons dit (voir N\u00b0\u00a0407)\u00a0; si, de fait, ainsi que nous l\u2019apprendra Alexandre\u00a0IV (voir bulle du 6\u00a0f\u00e9vrier 1256), il n\u2019a pas ex\u00e9cut\u00e9 la bulle du 6\u00a0juillet 1248 par laquelle Innocent\u00a0IV lui ordonnait de r\u00e9former le chapitre, il s\u2019ensuit que le pi\u00e9destal sur lequel <span class=\"small-caps\">Germain<\/span> avait voulu \u00e9lever cet \u00e9v\u00eaque s\u2019effondre de lui-m\u00eame, et qu\u2019il faut chercher ailleurs que dans ses relations avec le chapitre la cause de son empoisonnement.\n\nPendant son court \u00e9piscopat, Rainier n\u2019eut que de bons rapports avec la communaut\u00e9 maguelonaise\u00a0: il \u00e9rigea la vestiari\u00e9 en b\u00e9n\u00e9fice ind\u00e9pendant, qui ne fut confirm\u00e9 que par Cl\u00e9ment\u00a0IV, et, enfin, fit avec le pr\u00e9v\u00f4t un \u00e9change qu\u2019approuve la bulle d\u2019Innocent\u00a0IV, que nous commentons.\n\nSans doute, le Pape ne mentionne pas l\u2019objet de l\u2019\u00e9change\u00a0; mais il est assez clair pour ne laisser aucun doute \u00e0 celui qui est tant soit peu familiaris\u00e9 avec l\u2019histoire dioc\u00e9saine. Il s\u2019agit \u00e9videmment de l\u2019\u00e9change de Sainte-Marie de Melgueil, d\u00e9pendant du chapitre, contre les quatre \u00e9glises de Cournonterral, de Vendargues, de Castries et de Saint-Julien de Cazaligis\u00a0; et la bulle confirme l\u2019acte du 17\u00a0mars 1247 (n. s. 1248) par lequel l\u2019\u00e9v\u00eaque c\u00e9dait ces \u00e9glises.\n\n  L\u2019acquisition \u00e9tait-elle aussi bonne que semble l\u2019avoir dit Rainier dans son rapport au Souverain Pontife\u00a0? En r\u00e9alit\u00e9, le chapitre n\u2019aurait-il pas profit\u00e9 de son inexp\u00e9rience pour obtenir cet \u00e9change tout \u00e0 son avantage\u00a0? Nous serions tent\u00e9 de le croire.\n\nSainte-Marie de Melgueil \u00e9tait, il est vrai, l\u2019un des plus riches prieur\u00e9s du dioc\u00e8se, sinon le plus riche, \u2014 \u00e0 part Saint-Firmin\u00a0; et encore\u00a0! \u2014 La raison all\u00e9gu\u00e9e par Rainier pour l\u2019acqu\u00e9rir, le besoin d\u2019avoir toute juridiction temporelle et spirituelle dans Melgueil, nous para\u00eet assez pr\u00e9caire\u00a0; car il y avait l\u2019\u00e9glise de Saint-Jacques qui ne d\u00e9pendait, pour la collation, ni de l\u2019\u00e9v\u00eaque ni du chapitre.\n\nMais si, en face de cette seule \u00e9glise, nous pla\u00e7ons les quatre qu\u2019il c\u00e9da aux chanoines, il semble bien que ce fut un march\u00e9 de dupe. Nous ne savons ce que pouvait valoir le prieur\u00e9 de Saint-Julien de Cazaligis, pr\u00e8s de Cournonterral. Cette \u00e9glise dut dispara\u00eetre dans le cours du <span class=\"small-caps\">xiv<\/span>  <sup>e<\/sup> si\u00e8cle\u00a0; il est certain, en tout cas, que si nous avons trouv\u00e9, au <span class=\"small-caps\">xv<\/span>  <sup>e<\/sup> si\u00e8cle, une centaine d\u2019actes o\u00f9 elle est mentionn\u00e9e, nous n\u2019en avons rencontr\u00e9 aucun dans lequel on nomme un desservant, ni qui nous permette d\u2019en \u00e9tablir \u00e0 peu pr\u00e8s les revenus.\n\nIl en est de m\u00eame, il est vrai, pour Castries. Nous ignorons encore la valeur exacte de ce prieur\u00e9 au moyen \u00e2ge\u00a0; mais nous savons qu\u2019il \u00e9tait important, qu\u2019il avait un prieur, un cur\u00e9 et un secondaire.\n\nCournonterral et Vendargues, \u00e0 eux seuls, valaient Sainte-Marie de Melgueil. Le chapitre gagnait donc au change deux \u00e9glises, dont une consid\u00e9rable\u00a0: celle de Castries. Il ne pouvait se plaindre de l\u2019\u00e9v\u00eaque dominicain, qui accepta des conditions assez dures pour avoir une \u00e9glise dans le chef-lieu du comt\u00e9.\n\n[Rouquette-Villemagne]"
        },
        {
            "type": "apparat_critique",
            "value": "Les trois manuscrits\u00a0: <i>Innocencius<\/i>\u00a0; Germain\u00a0: <i>Innocentius<\/i>.",
            "children": {
                "numero_note": "a"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Les trois manuscrits et Germain\u00a0: <i>libencius<\/i>.",
            "children": {
                "numero_note": "b"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Bull. et Cart., 167\u00a0: <i>caucius<\/i>\u00a0; Cart., 166, et Germain\u00a0: <i>cautius<\/i>.",
            "children": {
                "numero_note": "c"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Cart., 166 et 167, et Germain\u00a0: <i>custodiri<\/i>\u00a0; Bull.\u00a0: <i>custodire<\/i>.",
            "children": {
                "numero_note": "d"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Bull. et Cart., 166\u00a0: <i>per diligentem<\/i>\u00a0; Cart., 167, et Germain\u00a0: <i>per ejus diligentem<\/i>.",
            "children": {
                "numero_note": "e"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Les trois manuscrits\u00a0: <i>tucior<\/i>\u00a0; Germain\u00a0: <i>tutior<\/i>.",
            "children": {
                "numero_note": "f"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Les trois manuscrits\u00a0: <i>anhelancium<\/i>\u00a0; Germain\u00a0: <i>anhelantium<\/i>.",
            "children": {
                "numero_note": "g"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Cart., 166 et 167, et Germain\u00a0: <i>recepisti<\/i>\u00a0; Bull.\u00a0: <i>recipist<\/i> avec abr\u00e9viation finale de <i>is<\/i>.",
            "children": {
                "numero_note": "h"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Les trois manuscrits\u00a0: <i>caucius<\/i>\u00a0; Germain\u00a0: <i>cautius<\/i>.",
            "children": {
                "numero_note": "i"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Les trois manuscrits\u00a0: <i>permutacionem<\/i>\u00a0; Germain\u00a0: <i>permutationem<\/i>.",
            "children": {
                "numero_note": "j"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Les trois manuscrits\u00a0: <i>confirmacionis<\/i>\u00a0; Germain\u00a0: <i>confirmationis<\/i>.",
            "children": {
                "numero_note": "k"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Les trois manuscrits\u00a0: <i>indignacionem<\/i>.",
            "children": {
                "numero_note": "l"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Bull.\u00a0: <i>VIII<\/i>\u00b0\u00a0; Cart., 166 et 167, et Germain\u00a0: <i>VIII<\/i>.",
            "children": {
                "numero_note": "m"
            }
        },
        {
            "type": "apparat_critique",
            "value": "Bull. et Cart., 166\u00a0: <i>idus<\/i>\u00a0; Cart., 167, et Germain\u00a0: <i>kalendas<\/i>.",
            "children": {
                "numero_note": "n"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<i>Bull. de Maguelone<\/i>, fol.\u00a018\u00a0v\u00b0",
            "children": {
                "numero_note": "0"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<i>Cart. de Maguelone<\/i>, reg.\u00a0F, fol.\u00a0166\u00a0v\u00b0, et 167\u00a0r\u00b0",
            "children": {
                "numero_note": "1"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<span class=\"small-caps\">Arnaud de Verdale<\/span>, \u00e9dit. <span class=\"small-caps\">Germain<\/span>, p.\u00a0265",
            "children": {
                "numero_note": "2"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<span class=\"small-caps\">F. Fabr\u00e8ge<\/span>, <i>Hist. de Maguelone<\/i>, t.\u00a0II, p.\u00a083",
            "children": {
                "numero_note": "3"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<span class=\"small-caps\">Germain<\/span>, <i>Maguelone sous ses \u00e9v\u00eaques<\/i>, p.\u00a059.",
            "children": {
                "numero_note": "4"
            }
        },
        {
            "type": "genre",
            "value": "Titre, g\u00e9n\u00e9ral (littere gratiose, cum filo serico)"
        },
        {
            "type": "destinataire",
            "value": "L'\u00e9v\u00eaque de Maguelonne"
        }
    ]
}