{
    "id": 20412,
    "name": "aposcripta-399",
    "type": "acte",
    "url": "http:\/\/telma-chartes.irht.cnrs.fr\/aposcripta\/notice\/20412",
    "project_slug": "aposcripta",
    "project_label": "APOSCRIPTA \u2013 Lettres des papes",
    "user": "lettrespontificalesadmin",
    "last_update": "2020-10-19 16:26:48",
    "licence": "Attribution - Pas d\u2019Utilisation Commerciale - Partage dans les M\u00eames Conditions 3.0 France (CC BY-NC-SA 3.0 FR) ",
    "licence_url": "https:\/\/creativecommons.org\/licenses\/by-nc-sa\/3.0\/fr\/",
    "elements": [
        {
            "type": "pape",
            "value": "Urbain V (1362-1370)"
        },
        {
            "type": "date_lieu",
            "value": "Avignon"
        },
        {
            "type": "date_libre",
            "value": "10 juillet 1364"
        },
        {
            "type": "analyse",
            "value": "Urbain V donne ordre \u00e0 l\u2019\u00e9v\u00eaque<a class=\"note\" href=\"#note-1\">1<\/a> et \u00e0 l\u2019inquisiteur<a class=\"note\" href=\"#note-2\">2<\/a> de Carcassonne de faire une enqu\u00eate sur des propos contre la foi et l\u2019autorit\u00e9 du pape tenus par sept routiers des Grandes Compagnies dont le mar\u00e9chal de France, Arnoul d\u2019Audrehem<a class=\"note\" href=\"#note-3\">3<\/a>, et les bourgeois de Carcassonne se sont empar\u00e9s r\u00e9cemment<a class=\"note\" href=\"#note-4\">4<\/a>. Arnoul leur avait promis la vie sauve et la d\u00e9livrance \u00e0 bref d\u00e9lai ; le lieutenant de l\u2019inquisiteur les fit enfermer dans les cachots de l\u2019\u00e9v\u00eaque."
        },
        {
            "type": "transcription",
            "value": "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\nVen. fratri Johanni, episcopo Carcassonen., ac dilecto filio .. ordinis fratrum predicatorum in partibus Carcassonen. inquisitori heretice pravitatis vel .. eius locumtenenti, salutem, etc. \u2014 Tua nobis, frater episcope, nuper notificavit epistola quod septem viros nefarios de illa gente dampnabili que Magna Societas appellatur, qui contra fidem catholicam presertim potestatem clavium a Salvatore nostro domino Ihesu Christo beato Petro apostolo et in eius personam suis successoribus traditam, quedam verba hereticalia pollutis labiis protulerunt, tibi per dilectum filium nobilem virum Arnulfum, dominum de Audenham, marescallum Francie ac carissimi in Christo filii nostri Caroli, regis Francie illustris, locumtenentem, qui eosdem viros propter alios eorum excessus ceperat, eis cum voluntate communitatum illarum partium sub protestatione proprii iuramenti eiusdem marescalli vite incolumitate promissa, traditos ad manum inquisitoris eiusdem sub fida per te custodia, ex eo quod super ipsorum captorum dimissione seu liberatione quam idem marescallus se promisisse dicebat, eodem locumtenente inquisitoris asserente illos sibi debere tradi, retinendos in tuis episcopalibus carceribus tenes inclusos, per te custodiendos donec super premissis nostre daremus beneplacitum voluntatis, quod a nobis cum multa instantia postulasti. Nos igitur nolentes, sicut nec velle debemus, quod propter premissa huiusmodi negotium sacre fidei quam attentissima cura debemus protegere negligatur, sed quod de verbis huiusmodi, que si vera sint proferentes eosdem reddunt vehementer de fide suspectos, diligentius inquiratur ; discretioni vestre in virtute sancte obedientie districte precipiendo mandamus quatinus de verbis eisdem ex vestro officio curetis solerter inquirere veritatem et ea que inde inveneritis nobis sub manu publica vel sub vestris sigillis clausa et nemini revelata quam cito commode poteritis destinare curetis ut mature providere possimus quid per vos agendum fuerit in premissis ; interimque tu, episcope, viros eosdem nulli, cuiuscumque status existat, presumas tradere sed illos caute facias custodiri donec aliud a nobis receperis in mandatis. \u2014 Datum Avinioni, vi idus iulii, anno secundo.\n"
        },
        {
            "type": "edition",
            "value": "Jean-Marie Vidal, <i>Bullaire de l\u2019Inquisition fran\u00e7aise au XIV<sup>e<\/sup> si\u00e8cle et jusqu'\u00e0 la fin du Grand Schisme<\/i>, Paris : Letouzey et An\u00e9, 1913, n. 236, p. 355-357"
        },
        {
            "type": "sources_manuscrites",
            "value": "<i>Reg. Vat.<\/i>, t. <span class=\"smal-caps\">ccxlvi<\/span>, fol. 269 v\u00b0",
            "children": {
                "type_source": "Registre de la chancellerie apostolique"
            }
        },
        {
            "type": "sources_manuscrites",
            "value": "Lecacheux, <i>op. cit.<\/i>, n. 1079."
        },
        {
            "type": "notes_historiques",
            "value": "Jean Fabre, cousin d\u2019Innocent VI, abb\u00e9 de Grammont ; \u00e9v\u00eaque du Puy, le 12 octobre 1356 ; transf\u00e9r\u00e9 \u00e0 Tortose, le 27 f\u00e9vrier 1357 ; et \u00e0 Carcassonne, le 10 janvier 1362. <i>Gall. christ.<\/i>, t. <span class=\"smal-caps\">vi<\/span>, col. 901 ; Eubel, <i>Hier.<\/i>, t. <span class=\"smal-caps\">i<\/span>, p. 91, 172, 231 ; Mahul, <i>Cartul.<\/i>, t. <span class=\"smal-caps\">v<\/span>, p. 454-455.",
            "children": {
                "numero_note": "1"
            }
        },
        {
            "type": "notes_historiques",
            "value": "Guillaume Chevalier (<i>Militis<\/i>), originaire d\u2019Angers, fit sa profession religieuse au couvent des dominicains de cette ville. La ma\u00eetrise en th\u00e9ologie lui fut conf\u00e9r\u00e9e sur l\u2019ordre de Cl\u00e9ment VI (31 juillet 1350) par Guillaume Munier. Son nom figure dans le catalogue des ma\u00eetres de l\u2019Universit\u00e9 d\u2019Angers. Denifle, <i>Archiv f\u00fcr Litteratur<\/i>, t. <span class=\"smal-caps\">ii<\/span>, p. 223, n. 123. En 1357, il \u00e9tait procureur g\u00e9n\u00e9ral de l\u2019ordre. <i>Reg. suppl.<\/i>, t. <span class=\"smal-caps\">xxvii<\/span>, fol. 308 v\u00b0. Puis il fut nomm\u00e9 inquisiteur de Carcassonne, \u00e0 la place d\u2019\u00c9tienne <i>de Ecclesia<\/i> (n. 223). Il eut \u00e0 proc\u00e9der contre les sept capitaines de routiers pris au si\u00e8ge de Peyriac (voir note 4). L\u2019\u00e9v\u00eaque de Carcassonne l\u2019accusa, en 1364, d\u2019empi\u00e9ter sur ses droits touchant la garde des murs de l\u2019Inquisition et le serment de fid\u00e9lit\u00e9 des officiers du tribunal (n. 251). Il usurpait aussi le droit de punir les blasph\u00e9mateurs non suspects d\u2019h\u00e9r\u00e9sie ; le pape lui ordonna, le 30 mai 1366, de ne pas d\u00e9passer les limites de sa comp\u00e9tence (n. 259). Par ordre du m\u00eame pontife, le tr\u00e9sorier de la Chambre lui paya, le 14 d\u00e9cembre de la m\u00eame ann\u00e9e, une somme de 40 florins. <i>Introitus et exitus<\/i>, t. <span class=\"smal-caps\">cccxxvi<\/span>, fol. 156. Le 22 juin 1369, Chevalier fut \u00e9lu patriarche de J\u00e9rusalem. Il mourut le 12 d\u00e9cembre 1371. Voir Qu\u00e9tif et Echard, <i>Script.<\/i>, t. <span class=\"smal-caps\">i<\/span>, p. 673 ; Denifle, <i>Chartul.<\/i>, t. <span class=\"smal-caps\">ii<\/span>, p. 660, 1183 ; Eubel, <i>Hier.<\/i>, t. <span class=\"smal-caps\">i<\/span>, p. 287.",
            "children": {
                "numero_note": "2"
            }
        },
        {
            "type": "notes_historiques",
            "value": "Arnoul d\u2019Audrehem, n\u00e9 vers 1305, devint capitaine du roi en Bretagne, en 1342 ; mar\u00e9chal de France, en 1351 ; lieutenant du roi en Poitou, Saintonge, Limousin, Angoumois, P\u00e9rigord, le 6 mars 1352 ; lieutenant en Normandie, en 1353 ; lieutenant en Artois, Picardie, Boulonnais, en 1355 ; lieutenant en Languedoc, en 1360. En novembre 1364, il fut remplac\u00e9 \u00e0 la lieutenance du Languedoc par le duc d\u2019Anjou. Il se d\u00e9mit peu apr\u00e8s de sa charge de mar\u00e9chal de France et, le 20 juin 1368, re\u00e7ut le titre purement honorifique de porte-oriflamme. Il mourut \u00e0 Saumur, en d\u00e9cembre 1370. Voir E. Molinier, <i>Etude sur la vie d\u2019Arnoul d\u2019Audrehem<\/i>, dans <i>Acad. des inscriptions et belles-lettres, M\u00e9moires<\/i>, etc., 1883, II<sup>e<\/sup> s\u00e9rie, t. <span class=\"smal-caps\">vi<\/span>, 1<sup>re<\/sup> part.",
            "children": {
                "numero_note": "3"
            }
        },
        {
            "type": "notes_historiques",
            "value": "Une bande de compagnons, ayant \u00e0 leur t\u00eate probablement Bertucat d\u2019Albret, s\u2019\u00e9tait empar\u00e9e par surprise, le 11 novembre 1363, du ch\u00e2teau de Peyriac, en Minervois. Le 16 novembre, Arnoul d\u2019Audrehem, \u00e0 la t\u00eate des milices de Carcassonne, des troupes de la province et de celles du vicomte de Narbonne, vint mettre le si\u00e8ge devant la place. Il fut oblig\u00e9 de le lever \u00e0 cause des intemp\u00e9ries. Les attaques recommenc\u00e8rent l\u2019ann\u00e9e suivante (1364), mais le ch\u00e2teau ne fut repris que dans la nuit du 18 au 19 juin, gr\u00e2ce \u00e0 l\u2019intervention des troupes de Montpellier. Voir sur ce fait d\u2019armes : le Thalamus Parvus de <i>Montpellier<\/i>, dans <i>Soci\u00e9t\u00e9 arch\u00e9ologique de Montpellier<\/i> (1840), p. 366 ; <i>Hist. de Lang.<\/i>, t. <span class=\"smal-caps\">ix<\/span>, p. 760-764 ; E. Molinier, <i>\u00c9tude sur la vie d\u2019A. Audrehem, loc. cit.<\/i>, p. 143-162 ; Denifle, <i>La d\u00e9solation des \u00e9glises, monast\u00e8res et h\u00f4pitaux en France pendant la guerre de Cent ans<\/i>, t. <span class=\"smal-caps\">ii<\/span>, p. 438.<br\/>Les routiers furent dispers\u00e9s ou massacr\u00e9s, \u00e0 l\u2019exception de sept capitaines (voir leurs noms au n. 246), qui furent faits prisonniers et incarc\u00e9r\u00e9s provisoirement \u00e0 Tr\u00e8bes. On d\u00e9couvrit bient\u00f4t que ces sept compagnons avaient tenu des propos contre la foi et d\u00e9nigr\u00e9 l\u2019autorit\u00e9 du pape. Le lieutenant de l\u2019inquisiteur exigea qu\u2019ils lui fussent livr\u00e9s. Second\u00e9 par la foule, dont l\u2019excitation \u00e9tait grande contre ces brigands, il obtint leur transfert dans la prison \u00e9piscopale de la cit\u00e9 de Carcassonne (n. 245). Une grave discussion s\u2019\u00e9leva aussit\u00f4t entre le mar\u00e9chal d\u2019Audrehem et l\u2019Inquisition \u00e0 propos de ce coup de force. Audrehem affirmait avoir promis par serment la vie sauve et la libert\u00e9 aux prisonniers, dont le commissaire inquisitorial pr\u00e9tendait faire justice, selon ses droits. L\u2019\u00e9v\u00eaque Jean Fabre en r\u00e9f\u00e9ra au pape. La r\u00e9ponse d\u2019Urbain V a \u00e9t\u00e9 analys\u00e9e plus haut. L\u2019Inquisition \u00e9tait ma\u00eetresse d\u2019enqu\u00eater, mais sous la haute direction du pontife lui-m\u00eame (10 juillet 1364). Peu de jours apr\u00e8s, le bruit courut dans le bourg de Carcassonne que certaines gens de la cit\u00e9 avaient tent\u00e9 de faire s\u2019\u00e9vader les prisonniers. Aussit\u00f4t deux mille Carcassonnais se pr\u00e9sentent en armes devant les remparts, mena\u00e7ant de mort quiconque favoriserait cette \u00e9vasion. Arnoul d\u2019Audrehem se trouvant alors dans le ch\u00e2teau de la cit\u00e9, cette \u00e9meute constituait une insulte au roi. Les consuls demand\u00e8rent des lettres de r\u00e9mission, qui, gr\u00e2ce au pape (13 septembre 1364 ; n. 245), furent accord\u00e9es. <i>Hist. de Lang.<\/i>, t. <span class=\"smal-caps\">x<\/span>, Preuves, col. 1329-1331. Le bruit d\u2019un complot pour la d\u00e9livrance des d\u00e9tenus avait sans doute quelque fondement, car le pape fit promulguer des censures dans les provinces de Toulouse et de Narbonne et dans les dioc\u00e8ses de Castres et d\u2019Albi, contre quiconque en renouvellerait l\u2019essai (18 octobre 1364 ; n. 247). Il statua \u00e9galement que les frais occasionn\u00e9s par l\u2019entretien et le proc\u00e8s des sept compagnons seraient r\u00e9partis entre les \u00e9v\u00eaques dans les dioc\u00e8ses desquels ils avaient perp\u00e9tr\u00e9 leurs forfaits (n. 246). L\u2019inquisiteur Guillaume Chevalier et un clerc du tribunal furent plus tard indemnis\u00e9s de leurs peines et de leurs d\u00e9pens, \u00e0 l\u2019aide des sommes recueillies (n. 255). Ce ne fut que le 19 octobre, au re\u00e7u de l\u2019enqu\u00eate <i>de infamia<\/i>, qu\u2019Urbain V ordonna \u00e0 l\u2019inquisiteur et \u00e0 l\u2019\u00e9v\u00eaque d\u2019entreprendre le proc\u00e8s \u00e0 fond. Il se r\u00e9servait toutefois d\u2019en examiner les actes, avant la conclusion (n. 248). Le m\u00eame jour, il autorisait l\u2019\u00e9v\u00eaque de Carcassonne et son vicaire g\u00e9n\u00e9ral \u00e0 conna\u00eetre des crimes le droit commun : sorcellerie, adult\u00e8res, homicides, incendies, violations d\u2019\u00e9glises, etc., dont les sept brigands s\u2019\u00e9taient rendus coupables en divers lieux du Languedoc (n. 249). Enfin, il faisait \u00e0 Jean Fabre la recommandation pressante de prendre les mesures n\u00e9cessaires pour emp\u00eacher l\u2019\u00e9vasion ou la mise en libert\u00e9 des captifs (n. 250). Lorsque l\u2019enqu\u00eate fut termin\u00e9e, Guillaume Chevalier en alla communiquer personnellement le dossier \u00e0 Urbain V. Apr\u00e8s m\u00fbre discussion, le pape prescrivit un suppl\u00e9ment d\u2019information, un nouvel appel de t\u00e9moins, un examen plus minutieux des pr\u00e9venus ; apr\u00e8s quoi, les juges prononceraient eux-m\u00eames (18 mars 1365 ; n. 254). Il en fut, sans doute, comme l\u2019ordonnait le pontife. Nous ne connaissons le sort que d\u2019un seul des inculp\u00e9s, Bidon de Puyguilhem, qui, convaincu d\u2019h\u00e9r\u00e9sie, fut condamn\u00e9 \u00e0 la prison perp\u00e9tuelle. Apr\u00e8s quelques ann\u00e9es de d\u00e9tention, contrit et retourn\u00e9 au giron de l\u2019\u00c9glise, il supplia Gr\u00e9goire XI de lui faire gr\u00e2ce. Le pape s\u2019en remit \u00e0 l\u2019inquisiteur de la lui accorder, s\u2019il y avait lieu, moyennant une p\u00e9nitence (14 mai 1371 ; n. 262). Il est \u00e0 pr\u00e9sumer que les six autres subirent la m\u00eame peine et, peut-\u00eatre, b\u00e9n\u00e9fici\u00e8rent de la m\u00eame gr\u00e2ce.",
            "children": {
                "numero_note": "4"
            }
        },
        {
            "type": "date",
            "value": "1364\/07\/10",
            "children": {
                "date_deb_annee": "1364",
                "date_deb_mois": "07",
                "date_deb_jour": "10"
            },
            "range": {
                "date_apres": 13640710,
                "date_avant": 13640710
            }
        },
        {
            "type": "genre",
            "value": "Bulle"
        },
        {
            "type": "edition_autre",
            "value": "Heinrich Denifle, <i>La d\u00e9solation des \u00e9glises, monast\u00e8res et h\u00f4pitaux en France pendant la guerre de cent ans<\/i>. II. <i>Documents relatifs au XIVe si\u00e8cle jusqu\u2019\u00e0 la mort de Charles V<\/i>, 1899, p. 439<a target=\"_blank\" href= \" https:\/\/archive.org\/stream\/ladsolationdes00deni#page\/438\/\"> [en ligne]<\/a>."
        }
    ]
}