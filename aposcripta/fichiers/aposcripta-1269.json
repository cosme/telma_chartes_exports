{
    "id": 24117,
    "name": "aposcripta-1269",
    "type": "acte",
    "url": "http:\/\/telma-chartes.irht.cnrs.fr\/aposcripta\/notice\/24117",
    "project_slug": "aposcripta",
    "project_label": "APOSCRIPTA \u2013 Lettres des papes",
    "user": "lettrespontificalesadmin",
    "last_update": "2020-10-19 16:27:23",
    "licence": "Attribution - Pas d\u2019Utilisation Commerciale - Partage dans les M\u00eames Conditions 3.0 France (CC BY-NC-SA 3.0 FR) ",
    "licence_url": "https:\/\/creativecommons.org\/licenses\/by-nc-sa\/3.0\/fr\/",
    "elements": [
        {
            "type": "pape",
            "value": "Alexandre III (1159-1181)"
        },
        {
            "type": "date_lieu",
            "value": "n.c."
        },
        {
            "type": "date_libre",
            "value": "12\u00a0f\u00e9vrier 1166"
        },
        {
            "type": "date",
            "value": " 1166\/02\/12",
            "children": {
                "date_deb_annee": "1166",
                "date_deb_mois": "02",
                "date_deb_jour": "12"
            },
            "range": {
                "date_apres": 11660212,
                "date_avant": 11660212
            }
        },
        {
            "type": "analyse",
            "value": "Alexandre\u00a0III ordonne aux chanoines de maintenir l\u2019accord conclu entre le chapitre et l\u2019\u00e9v\u00eaque\u00a0; si celui-ci le viole, ils doivent n\u00e9anmoins s\u2019y conformer pour leur part, et ne pas ob\u00e9ir \u00e0 l\u2019\u00e9v\u00eaque. Il r\u00e8gle aussi que personne ne sera admis dans la communaut\u00e9 sans le consentement du chapitre."
        },
        {
            "type": "transcription",
            "value": "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\nAlexander episcopus, servus servorum Dei, dilectis filiis Magalonensis Ecclesie canonicis, salutem et apostolicam benedictionem.Devotionis et fidei vestre fervorem, quo Ecclesie Romane et nobis, hoc maxime tempore, adheretis, sollicita mentis intentione pensantes, et illius obsequii, quod nobis sepius et oportunius impendistis, nequaquam immemores existentes, commodis et incrementis vestris libenter intendimus, et ad hec, quantum cum Deo possumus, ferventius aspiramus.Inde siquidem est quod, paci et tranquillitati vestre studium et operam impendere cupientes, universitati vestre auctoritate presentium prohibemus, ut ad ea que contra compositionem, inter vos et venerabilem fratrem nostrum, episcopum vestrum, factam, fuerint, vel scriptis apostolicis, vobis aut Ecclesie vestre indultis, debeant aliquatenus obviare, occasione obedientie ab eodem episcopo nullatenus constringamini, aut quomodolibet veniatis\u00a0: hoc enim non esset veram obedientiam observare, sed eidem potius contraire.Si vero jam dictus episcopus contra hec aliquo tempore venire temptaverit, vos non minus ea que in prelibata concordia et in scriptis Romane Ecclesie continentur libere exequamini, nullius in hiis obedientie vinculis episcopo memorato constricti.Inhibemus etiam ut nullum in fratrem et canonicum sine communi fratrum consensu, aut sanioris partis, ad alicujus instantiam, in posteruma recipiatis.Datum Rome, apud Sanctum Petrum, ii idus februarii.\n"
        },
        {
            "type": "edition",
            "value": "Julien Rouquette, Augustin Villemagne, <i>Bullaire de l'\u00c9glise de Maguelone<\/i>, Paris, Montpellier : Picard, Valat, 1911-1914, I, n. 79-LII, p. 119-120"
        },
        {
            "type": "remarques",
            "value": "    Date. \u2014 Il n\u2019y a pas de doute que cette bulle ait \u00e9t\u00e9 adress\u00e9e aux chanoines de Maguelone, lorsque Bernard \u00e9tait pr\u00e9v\u00f4t. <span class=\"small-caps\">Germain<\/span> en fixe la date au 12\u00a0f\u00e9vrier 1166-1167\u00a0; <span class=\"small-caps\">Jaff\u00e9<\/span>, toujours probablement sur le m\u00eame motif que dans la bulle pr\u00e9c\u00e9dente, au 12\u00a0f\u00e9vrier 1166. Nous le suivons pour la raison d\u00e9j\u00e0 expos\u00e9e. En effet, les difficult\u00e9s devaient \u00eatre assez graves au sein du chapitre pour d\u00e9terminer le Pape \u00e0 dire aux chanoines de ne pas ob\u00e9ir \u00e0 leur \u00e9v\u00eaque, si celui-ci violait les accords conclus pr\u00e9c\u00e9demment.\n\nAnalyser, mentionner m\u00eame les accords conclus \u00e0 cette \u00e9poque entre l\u2019\u00e9v\u00eaque et le pr\u00e9v\u00f4t, nous para\u00eet sortir du plan que nous nous sommes trac\u00e9 en ajoutant ces notes historiques aux documents pontificaux. Il faudrait \u00e0 chaque instant citer le <i>Cartulaire de Maguelone<\/i> et \u00e9crire une histoire du dioc\u00e8se d\u2019apr\u00e8s les actes originaux encore in\u00e9dits. Nous aurons atteint notre but si le lecteur peut saisir la port\u00e9e des bulles.\n\nUn seul point m\u00e9rite donc de nous arr\u00eater\u00a0: le mode de recrutement des chanoines. Il est bien probable que, depuis Urbain\u00a0II, un chanoine n\u2019\u00e9tait admis dans la communaut\u00e9 qu\u2019apr\u00e8s un vote de tous les membres du chapitre. Alexandre\u00a0III aurait donc simplement sanctionn\u00e9 une coutume l\u00e9gitimement \u00e9tablie. Dans tous les cas, ce statut fut fid\u00e8lement observ\u00e9 aussi longtemps que Maguelone resta si\u00e8ge de l\u2019\u00e9v\u00each\u00e9, c\u2019est-\u00e0-dire jusqu\u2019\u00e0 la s\u00e9cularisation du chapitre et son transfert \u00e0 Montpellier.\n\nEn entrant dans la communaut\u00e9 le chanoine devait apporter un trousseau. (Cf. <span class=\"small-caps\">J. Rouquette<\/span>, <i>La d\u00e9cimarie du vestiaire<\/i>, dans <i>Revue Hist. du dioc\u00e8se de Montpellier<\/i>, 1<sup>re<\/sup> ann\u00e9e, p.\u00a0213). En m\u00eame temps que le chapitre d\u00e9cidait l\u2019admission du nouveau chanoine, celui-ci devait offrir une \u00e9toffe d\u2019or, <i>pannus aureus<\/i>, d\u2019une valeur de 120 sols tournois (Cf. <span class=\"small-caps\">Germain<\/span>, <i>Statuts de Maguelone<\/i>, dans <i>Maguelone sous ses \u00e9v\u00eaques<\/i>, p.\u00a0286). Nous pouvons affirmer que g\u00e9n\u00e9ralement nos chanoines s\u2019acquitt\u00e8rent de cette dette, non pas en nature, mais en argent.\n\n[Rouquette-Villemagne]"
        },
        {
            "type": "apparat_critique",
            "value": "Ms. et Germain\u00a0: <i>imposterum<\/i>.",
            "children": {
                "numero_note": "a"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<i>Priv. de Maguelone<\/i>, fol.\u00a017\u00a0v\u00b0",
            "children": {
                "numero_note": "0"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<span class=\"small-caps\">Germain<\/span>, <i>Maguelone sous ses \u00e9v\u00eaques<\/i>, p.\u00a0171",
            "children": {
                "numero_note": "1"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<span class=\"small-caps\">Jaff\u00e9<\/span> et <span class=\"small-caps\">Wattenbach<\/span>, n\u00b0\u00a011262",
            "children": {
                "numero_note": "2"
            }
        },
        {
            "type": "bibliographie_generale",
            "value": "<span class=\"small-caps\">F. Fabr\u00e8ge<\/span>, <i>Hist. de Maguelone<\/i>, t.\u00a0I, p.\u00a0288.",
            "children": {
                "numero_note": "3"
            }
        },
        {
            "type": "genre",
            "value": "Bulle"
        }
    ]
}