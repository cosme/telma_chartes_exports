...under development...


# Details

This README.md file contains explanation about JSON exports from telma-chartes.irht.cnrs.fr

General concerns are described below

For each database, please read the README.md of the related directory

## Directories

Each directory is name after the database slug on telma-chartes.irht.cnrs.fr
Each one contains a .json file that list every published records and fields

## Licence

All records are published under the CC BY-NC-SA 3.0 licence
For details, please read the following page https://creativecommons.org/licenses/by-nc-sa/3.0/fr

## JSON Files

_source :
value : name/title of the record/oject 
status : record/object id published (2) or not (1). Every record/ojbect in this repo has been tagged as "puclished" by its owner
user : username of the record's creator
object_id => id of the record/object
object_name => name/title of the record/object
object_type => type of the record/object (usually "acte")
object_url => URL of the record/object
gitlab => URL of the GitLab repo
project_id : database/project id 
project_slug : database/project slug, as it is used in the URL : telma-chartes.irht.cnrs.fr/{project_slug}
project_label : real Label of the database/project
licence : creative common licence  (plain text)
licence_url : creative common licence URL
last_update : date (Y-m-d) of the last update
